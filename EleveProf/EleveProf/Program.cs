﻿using System;
using System.Collections.Generic;
using System.Text;
using EleveProfLibrary;
namespace EleveProf
{
    class Program
    {
        static void Main(string[] args)
        {
            //Afficher les accents et autres caractères
            Console.OutputEncoding = Encoding.UTF8;
            string rn = Environment.NewLine; // Saut de ligne

            /*
             * CLASSES
             */
            Classe wis1 = new Classe(6, "wis1");
            Classe wis2 = new Classe(7, "wis2");

            GestionClasseADO gestionClasse = new GestionClasseADO();
            Console.WriteLine("Affichage liste de classes");
            //--Create 
            gestionClasse.AjouterClasse(wis1);
            gestionClasse.AjouterClasse(wis2);
            //--Read
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            { 
                Console.WriteLine(classe.DescriptionSigle);
            }   
            //--Update
            wis1 = new Classe(6, "wis0");
            gestionClasse.ModifierClasse(wis1);
            //--Delete
            gestionClasse.SupprimerClasse(7);

            Console.WriteLine(rn);

            /*
             * PROFS 
             */
            Prof nouveauProf1 = new Prof(11, "Bompard", "Jean-Charles", new DateTime(1972, 11, 21), Prof.TypeStatut.Intervenant, 3900);
            Prof nouveauProf2 = new Prof(12, "Isoird", "Gael", new DateTime(1975, 11, 22), Prof.TypeStatut.Permanent, 4200);

            GestionPersonneADO gestionProf = new GestionPersonneADO();
            Console.WriteLine("Affichage liste de profs");
            //--Create
            gestionProf.AjouterProf(nouveauProf1);
            gestionProf.AjouterProf(nouveauProf2);
            //--Read
            List<Prof> listProfs = gestionProf.ChargerProfs();
            foreach (Prof prof in listProfs)
            {
                Console.WriteLine(prof.Identite);
            }
            //--Update
            nouveauProf1 = new Prof(11, "Bompard", "Jean-Luc", new DateTime(1972, 11, 21), Prof.TypeStatut.Intervenant, 4100);
            gestionProf.ModifierProf(nouveauProf1);
            //--Delete
            gestionProf.SupprimerProf(12);

            Console.WriteLine(rn);

            /*
             * ELEVES
             */
            Classe wis3 = new Classe(8, "wis3");
            Classe wis4 = new Classe(9, "wis4");
            gestionClasse.AjouterClasse(wis3); //Pour check si modif sur l'eleve car (obj)wis2 est effacé.

            Eleve nouvelEleve1 = new Eleve(13, "Laverune", "Phil", new DateTime(1972, 10, 22), Eleve.TypeFormation.Initiale, (float)12.50, wis1);
            Eleve nouvelEleve2 = new Eleve(14, "Laverune", "Philippe", new DateTime(1973, 9, 22), Eleve.TypeFormation.Initiale, (float)13.50, wis1);

            GestionPersonneADO gestionEleve = new GestionPersonneADO();
            Console.WriteLine("Affichage liste d'élèves");
            //--Create
            gestionEleve.AjouterEleve(nouvelEleve1);
            gestionEleve.AjouterEleve(nouvelEleve2);
            //--Read
            List<Eleve> listEleves = gestionEleve.ChargerEleves();
            foreach (Eleve eleve in listEleves)
            {
                Console.WriteLine(eleve.Identite);
            }
            //--Update
            nouvelEleve1 = new Eleve(13, "Laguna", "Bernard", new DateTime(1972, 10, 22), Eleve.TypeFormation.Alternance, (float)17.50, wis3);
            gestionEleve.ModifierEleve(nouvelEleve1);
            //--Delete
            gestionEleve.SupprimerEleve(14);

            Console.WriteLine(rn);

            /*
             * MATIERES
             */
            Matiere matiere1 = new Matiere(6, "Economie", 35, wis1);
            Matiere matiere2 = new Matiere(7, "SQL", 25, wis2);

            GestionMatiereADO gestionMatiere = new GestionMatiereADO();
            Console.WriteLine("Affichage liste de matières");

            //--Create
            //gestionMatiere.AjouterMatiere();
            //gestionMatiere.AjouterMatiere();
            //--Read
            List<Matiere> listMatieres = gestionMatiere.ChargerMatieres();
            foreach (Matiere matiere in listMatieres)
            {
                Console.WriteLine(matiere.FicheMatiere);
            }
            //--Update
            //matiere1 = new Matiere(6, "UML", 35, wis1);
            //gestionMatiere //TODO Suite

        }
    }
}
