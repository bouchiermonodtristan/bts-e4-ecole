﻿using System;
namespace EleveProfLibrary
{
    public class Classe //Notes : ok
    {
        #region Attributs
        private int id;
        private string sigle;
        #endregion

        #region Constructeur
        public Classe(int id, string sigle)
        {
            Id = id;
            Sigle = sigle;
        }
        #endregion

        #region Accesseurs - Clé primaire
        public int Id
        {
            get { return id; }
            set
            {
                if (value < 1)
                    throw new Exception("L'id doit être supérieur ou égale à 1");

                id = value;
            }
        }
        public string Sigle
        {
            get { return sigle; }
            set
            {
                if (value == "")
                    throw new Exception("Le sigle ne doit pas être vide");

                sigle = value;
            }
        }
        #endregion

        //GetDescription
        public string DescriptionSigle
        {
            get
            {
                return string.Format("id : {0}, sigle : {1}",
                                     Id, Sigle);
            }
        }
    }
}
