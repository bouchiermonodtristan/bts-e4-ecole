﻿using System;
using System.Collections.Generic;

namespace EleveProfLibrary
{
    public class Ecole
    {
        private List<Classe> listeClasses = new List<Classe>();
        private List<Personne> listePersonnes = new List<Personne>();

        //private List<Modele> listeModeles = new List<Modele>();
        //private List<Constructeur> listeConstructeurs = new List<Constructeur>();


        public Classe AjouterClasse(Classe classe)
        {
            Classe newClasse = RechercherClasse(classe.Id);
            if (newClasse == null)
            {
                listeClasses.Add(classe);
                newClasse = classe;
            }
            return newClasse;
        }

        public Classe RechercherClasse(int id)
        {
            return listeClasses.Find(delegate (Classe classe)
            {
                return classe.Id == id;
            });
        }

    }
}
