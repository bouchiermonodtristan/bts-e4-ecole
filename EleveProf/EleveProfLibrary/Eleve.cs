﻿using System;
namespace EleveProfLibrary
{
    public class Eleve : Personne
    {

        #region Attributs
        //Primary key
        public enum TypeFormation { 
            Initiale = 0, 
            Alternance = 1
        };
        private TypeFormation formation;
        private float moyenne;

        //Foreign key
        private Classe classe;
        #endregion



        #region Accesseurs
        public float Moyenne
        {
            get { return moyenne; }
            set
            {
                if (value < 0 || value > 20)
                    throw new Exception("La moyenne ne peut pas etre negatif ou dépasser 20");
                moyenne = value;
            }
        }
        public TypeFormation Formation
        {
            get { return formation; }
            set { formation = value; }
        }

        //Foreign K
        public Classe ClasseEleve 
        {
            get => classe;
            set
            {
                classe = value ?? throw new Exception("La classe ne peut pas être null");
            }
        }

        #endregion

        #region Constructeur
        public Eleve(int id, string nom, string prenom, DateTime dateDeNaissance, TypeFormation formation, float moyenne, Classe classe)
            : base(id, nom, prenom, dateDeNaissance)
        {
            Formation = formation;
            Moyenne = moyenne;
            ClasseEleve = classe; //FK
        }
        public Eleve(int id, string nom, string prenom, DateTime dateDeNaissance, TypeFormation formation, float moyenne)
            : base(id, nom, prenom, dateDeNaissance)
        {
            Formation = formation;
            Moyenne = moyenne;
        }
        #endregion

        #region Constructeurs et Methodes
        //TODO Dee check Personne
        public override string Identite
        {
            get { return string.Format("Eleve : {0}, formation {1}, moyenne : {2}", base.Identite, formation, moyenne); }
        }
        #endregion

    }
}
