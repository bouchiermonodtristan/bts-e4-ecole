﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace EleveProfLibrary
{
    public class GestionBaseADO
    {
        #region Attributes
        // Chaine de connection à la BD
        private string connectionString;
        // Connection à la BD
        private MySqlConnection connection;
        #endregion

        #region Builders
        public GestionBaseADO()
        {
            this.connectionString = ADOConstants.ADO_CONNEXION_STRING;
            connection = new MySqlConnection(connectionString);
        }
        #endregion


        #region Connection
        protected void Open()
        {
            if (connection != null && connection.State == ConnectionState.Closed)
                connection.Open();
        }
        public bool IsOpenned()
        {
            if (connection != null && connection.State == ConnectionState.Open)
                return true;

            return false;
        }
        protected void Close()
        {
            if (connection != null && connection.State == ConnectionState.Open)
                connection.Close();
        }

        protected MySqlConnection GetSqlConnection() { return this.connection; }
        #endregion
    }
}
