﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace EleveProfLibrary //Notes : ok
{
    public class GestionClasseADO : GestionBaseADO
    {
        #region Builders
        public GestionClasseADO()
            : base()
        {
        }
        #endregion

        public List<Classe> ChargerClasses()
        {
            List<Classe> liste = new List<Classe>();
            try
            {
                Open();

                string query = "SELECT id_classe, sigle FROM Classe";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        Classe classe = new Classe(reader.GetInt32(0), reader.GetString(reader.GetOrdinal("sigle")));

                        // Ajout de la classe dans la liste
                        if (classe != null)
                            liste.Add(classe);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
                reader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }

            // On retourne la liste des classes
            return liste;
        }



        public bool AjouterClasse(Classe classe)
        {

            bool result = true;

            try
            {
                Open();
                string query = @"INSERT INTO Classe VALUES (@idCla, @sigle)";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idCla", classe.Id);
                cmd.Parameters.AddWithValue("@sigle", classe.Sigle);

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                //result = false;
            }
            finally
            {
                Close();
            }

            return result;
        }
        public bool ModifierClasse(Classe classe)
        {
            int result = 0;
            try
            {
                Open();
                string query = @"UPDATE Classe SET sigle = @sigle WHERE id_classe = @id";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());
                cmd.Parameters.AddWithValue("@id", classe.Id);
                cmd.Parameters.AddWithValue("@sigle", classe.Sigle);

                result = cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }


        public bool SupprimerClasse(int id)
        {
            int result = 0;
            try
            {
                Open();

                string query = "DELETE FROM Classe WHERE id_classe = @id";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());
                cmd.Parameters.AddWithValue("@id", id);
                result = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }
    }
}
