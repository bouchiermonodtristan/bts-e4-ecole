﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace EleveProfLibrary //TODO Dee avec OBJET PROF et CLASSE
{
    public class GestionMatiereADO : GestionBaseADO
    {
        #region Builders
        public GestionMatiereADO()
            : base()
        {
        }
        #endregion

        public List<Matiere> ChargerMatieres()
        {
            List<Matiere> liste = new List<Matiere>();
            try
            {
                Open();

                string query = "SELECT id_matiere, nom_mat, heure_mat, M.ID_PERSONNE, M.ID_CLASSE" +
                    " FROM MATIERE as M, PROFESSEUR as P, CLASSE as C" +
                    " WHERE P.ID_PERSONNE = M.ID_PERSONNE AND C.ID_CLASSE = M.ID_CLASSE";
                //string query = "SELECT id_matiere, M.nom_mat, C.sigle, heure_mat, P.prenom, P.nom" +
                //" FROM MATIERE M, PERSONNE P, CLASSE C, PROFESSEUR PR" +
                //   " WHERE P.ID_PERSONNE = PR.ID_PERSONNE" +
                //" AND M.id_personne = P.id_personne" +
                //" AND C.id_classe = M.id_classe";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {

                        int profId = reader.GetInt32(3);
                        Prof ThisProf = new Prof(1, "test", "test", DateTime.Now, 9999);


                        GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                        List<Prof> listProfs = gestionPersonne.ChargerProfs();
                        foreach (Prof prof in listProfs)
                        {
                            if(prof.Id == profId)
                            {
                                ThisProf = prof;
                            }
                        }

                        Matiere matiere = new Matiere(
                        reader.GetInt32(0),
                        reader.GetString(reader.GetOrdinal("nom_mat")),
                        reader.GetInt32(2),
                        ThisProf,
                        new Classe(reader.GetInt32(4), "TEST")
                        );

                        // Ajout du produit dans la liste
                        if (matiere != null)
                            liste.Add(matiere);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
                reader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }

            // On retourne la liste des classes
            return liste;
        }

        public bool AjouterMatiere(Matiere matiere)
        {

            bool result = true;

            try
            {
                Open();
                string query = @"INSERT INTO Matiere VALUES (@idMat, @nomMat, @heureMat, @idPers, @idClasse)";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idMat", matiere.Id);
                cmd.Parameters.AddWithValue("@nomMat", matiere.Nom);
                cmd.Parameters.AddWithValue("@heureMat", matiere.Heure);
                cmd.Parameters.AddWithValue("@idPers", matiere.ProfDeMatiere.Id);
                cmd.Parameters.AddWithValue("@idClasse", matiere.ClasseAyantMatiere.Id);

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                //result = false;
            }
            finally
            {
                Close();
            }

            return result;
        }

        public bool ModifierMatiere(Matiere matiere)
        {
            int result = 0;
            try
            {
                Open();
                string query = @"UPDATE Matiere SET nom_mat = @nom, heure_mat = @heure, id_personne = @prof, id_classe = @classe WHERE id_matiere = @id";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());
                cmd.Parameters.AddWithValue("@id", matiere.Id);
                cmd.Parameters.AddWithValue("@nom", matiere.Nom);
                cmd.Parameters.AddWithValue("@heure", matiere.Heure);
                cmd.Parameters.AddWithValue("@classe", matiere.ClasseAyantMatiere.Id);
                cmd.Parameters.AddWithValue("@prof", matiere.ProfDeMatiere.Id);

                result = cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }
    }
}
