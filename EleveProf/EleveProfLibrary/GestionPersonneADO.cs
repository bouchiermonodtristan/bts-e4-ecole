﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace EleveProfLibrary
{
    public class GestionPersonneADO : GestionBaseADO
    {
        #region Builders
        public GestionPersonneADO()
            : base()
        {
        }
        #endregion

        #region Personne
        public List<int> ChargerPersonne()
        {
            List<int> liste = new List<int>();
            try
            {
                Open();

                //TODO Dee dit : check si c bon
                string query = "" +
                    "SELECT ID_PERSONNE" +
                    " FROM PERSONNE";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {

                        //Ajout du produit dans la liste
                        liste.Add(reader.GetInt32(0));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
                reader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }

            // On retourne la liste
            return liste;
        }
        #endregion
        #region Professeur
        public List<Prof> ChargerProfs()
        {
            List<Prof> liste = new List<Prof>();
            try
            {
                Open();

                //TODO Dee dit : check si c bon
                string query = "" +
                	"SELECT PR.ID_PERSONNE, `NOM`, `PRENOM`, `DATENAISS`, `STATUS`, `SALAIRE`" +
                	" FROM PERSONNE P, PROFESSEUR PR" +
                	" WHERE P.ID_PERSONNE = PR.ID_PERSONNE";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        //PourGetOrdinal, assign values
                        var nom = reader.GetOrdinal("nom");
                        var prenom = reader.GetOrdinal("prenom");

                        Prof prof = new Prof(
                        reader.GetInt32(0),
                        reader.GetString(nom),
                        reader.GetString(prenom),
                        reader.GetDateTime(3),
                        (Prof.TypeStatut)reader.GetInt16(4),
                        reader.GetInt32(5)
                        );

                        //Ajout du produit dans la liste
                        if (prof != null)
                            liste.Add(prof);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
                reader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }

            // On retourne la liste
            return liste;
        }

        public bool AjouterProf(Prof prof)
        {

            bool result = true;

            try
            {
                Open();
                string query = @"
                INSERT INTO PERSONNE VALUES (@idPers, @nom, @prenom, @ddn);";

                string query2 = @"
                 INSERT INTO PROFESSEUR VALUES (@idPers, @status, @salaire);";

                string queryFinal = query + query2;
                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idPers", prof.Id);
                cmd.Parameters.AddWithValue("@nom", prof.Nom);
                cmd.Parameters.AddWithValue("@prenom", prof.Prenom);
                cmd.Parameters.AddWithValue("@ddn", prof.DateDeNaissance);
                cmd.Parameters.AddWithValue("@status", prof.Statut);
                cmd.Parameters.AddWithValue("@salaire", prof.Salaire);

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                //result = false;
            }
            finally
            {
                Close();
            }

            return result;
        }

        public bool ModifierProf(Prof prof)
        {
            int result = 0;
            try
            {
                Open();

                string query = @"
                UPDATE PERSONNE SET 
                    nom = @nom,
                    prenom = @prenom,
                    datenaiss = @ddn
                WHERE ID_PERSONNE = @idPers; ";

                string query2 = @"
                 UPDATE PROFESSEUR SET 
                    status = @status, 
                    salaire = @salaire
                WHERE ID_PERSONNE = @idPers; ";

                string queryFinal = query + query2;

                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idPers", prof.Id);
                cmd.Parameters.AddWithValue("@nom", prof.Nom);
                cmd.Parameters.AddWithValue("@prenom", prof.Prenom);
                cmd.Parameters.AddWithValue("@ddn", prof.DateDeNaissance);
                cmd.Parameters.AddWithValue("@status", prof.Statut);
                cmd.Parameters.AddWithValue("@salaire", prof.Salaire);

                result = cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }

        public bool SupprimerProf(int id)
        {
            int result = 0;
            try
            {
                Open();

                string query1 = "DELETE FROM Professeur WHERE ID_PERSONNE = @id; ";
                string query2 = "DELETE FROM Personne WHERE ID_PERSONNE = @id";

                string queryFinal = query1 + query2;
                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());
                cmd.Parameters.AddWithValue("@id", id);
                result = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }
        #endregion

        #region Eleve
        public List<Eleve> ChargerEleves()
        {
            List<Eleve> liste = new List<Eleve>();
            try
            {
                Open();

                //TODO Dee dit : check si c bon
                string query = "" +
                    "SELECT E.ID_PERSONNE, `NOM`, `PRENOM`, `DATENAISS`, `SIGLE`, `FORMATION`, `MOYENNE`, E.`ID_CLASSE`" +
                    " FROM PERSONNE P, ELEVE E, CLASSE C " +
                    " WHERE P.ID_PERSONNE = E.ID_PERSONNE " +
                    " AND C.ID_CLASSE = E.ID_CLASSE";
                MySqlCommand cmd = new MySqlCommand(query, GetSqlConnection());

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        //PourGetOrdinal, assign values
                        var sigle = reader.GetOrdinal("sigle");
                        var prenom = reader.GetOrdinal("prenom");
                        var nom = reader.GetOrdinal("nom");
                        var moyenne = reader.GetOrdinal("moyenne");
                        var formation = reader.GetOrdinal("formation");



                        Eleve eleve = new Eleve(
                        reader.GetInt32(0),
                        reader.GetString(nom),
                        reader.GetString(prenom),
                        reader.GetDateTime(3),
                        (Eleve.TypeFormation)reader.GetInt32(formation),
                        reader.GetFloat(moyenne),
                        new Classe(reader.GetInt32(7), "TEST")
                        );

                        //Ajout dans la liste
                        if (eleve != null)
                            liste.Add(eleve);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
                reader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }

            // On retourne la liste
            return liste;
        }

        public bool AjouterEleve(Eleve eleve)
        {

            bool result = true;

            try
            {
                Open();
                string query = @"
                INSERT INTO PERSONNE VALUES (@idPers, @nom, @prenom, @ddn);";

                string query2 = @"
                 INSERT INTO ELEVE VALUES (@idPers, @moyenne, @formation, @idclasse);";

                string queryFinal = query + query2;
                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idPers", eleve.Id);
                cmd.Parameters.AddWithValue("@nom", eleve.Nom);
                cmd.Parameters.AddWithValue("@prenom", eleve.Prenom);
                cmd.Parameters.AddWithValue("@ddn", eleve.DateDeNaissance);
                cmd.Parameters.AddWithValue("@moyenne", eleve.Moyenne);
                cmd.Parameters.AddWithValue("@formation", eleve.Formation);
                cmd.Parameters.AddWithValue("@idclasse", eleve.ClasseEleve.Id);

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
                //result = false;
            }
            finally
            {
                Close();
            }

            return result;
        }

        public bool ModifierEleve(Eleve eleve)
        {
            int result = 0;
            try
            {
                Open();
                string query = @"
                UPDATE PERSONNE SET 
                    nom = @nom,
                    prenom = @prenom,
                    datenaiss = @ddn
                WHERE ID_PERSONNE = @idPers; ";

                string query2 = @"
                 UPDATE ELEVE SET 
                    moyenne = @moyenne, 
                    formation = @formation,
                    id_classe = @idClasse
                WHERE ID_PERSONNE = @idPers; ";

                string queryFinal = query + query2;

                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());

                cmd.Parameters.AddWithValue("@idPers", eleve.Id);
                cmd.Parameters.AddWithValue("@nom", eleve.Nom);
                cmd.Parameters.AddWithValue("@prenom", eleve.Prenom);
                cmd.Parameters.AddWithValue("@ddn", eleve.DateDeNaissance);
                cmd.Parameters.AddWithValue("@moyenne", eleve.Moyenne);
                cmd.Parameters.AddWithValue("@formation", eleve.Formation);
                cmd.Parameters.AddWithValue("@idClasse", eleve.ClasseEleve.Id);

                result = cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }

        public bool SupprimerEleve(int id)
        {
            int result = 0;
            try
            {
                Open();

                string query1 = "DELETE FROM Professeur WHERE ID_PERSONNE = @id; ";
                string query2 = "DELETE FROM Eleve WHERE ID_PERSONNE = @id";

                string queryFinal = query1 + query2;
                MySqlCommand cmd = new MySqlCommand(queryFinal, GetSqlConnection());
                cmd.Parameters.AddWithValue("@id", id);
                result = cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Close();
            }
            return false;
        }
        #endregion
    }
}
