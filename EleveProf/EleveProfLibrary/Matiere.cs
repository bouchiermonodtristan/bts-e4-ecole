﻿using System;
namespace EleveProfLibrary
{
    public class Matiere
    {
        #region Attributs
        private int id;
        private string nom;
        private int heure;

        //Foreign K
        public Personne personne;
        public Classe classe;
        #endregion

        #region Constructeur
        public Matiere(int id, string nom, int heure, Personne personne, Classe classe)
        {
            Id = id;
            Nom = nom;
            Heure = heure;
            ProfDeMatiere = personne; //FK
            ClasseAyantMatiere = classe; //FK
        }
        public Matiere(int id, string nom, int heure, Classe classe)
        {
            Id = id;
            Nom = nom;
            Heure = heure;
            ClasseAyantMatiere = classe; //FK
        }
        public Matiere(int id, string nom, int heure, Personne personne)
        {
            Id = id;
            Nom = nom;
            Heure = heure;
            ProfDeMatiere = personne; //FK
        }
        public Matiere(int id, string nom, int heure)
        {
            Id = id;
            Nom = nom;
            Heure = heure;
        }
        #endregion

        #region Accesseurs
        public int Id
        {
            get { return id; }
            set
            {
                if (value < 1)
                    throw new Exception("L'id doit être supérieur ou égale à 1");

                id = value;
            }
        }
        public string Nom
        {
            get { return nom; }
            set
            {
                if (value == "")
                    throw new Exception("Le nom ne doit pas être vide");

                nom = value;
            }
        }
        public int Heure
        {
            get { return heure; }
            set
            {
                if (value < 1)
                    throw new Exception("Le nombre d'heures doit être supérieur ou égale à 1");

                heure = value;
            }
        }
        public Personne ProfDeMatiere
        {
            get => personne;
            set
            {
                personne = value ?? throw new Exception("La personne ne peut pas être null");
            }
        }
        public Classe ClasseAyantMatiere
        {
            get => classe;
            set
            {
                classe = value ?? throw new Exception("La classe ne peut pas être null");
            }
        }
        #endregion
        #region Affichage
        public string FicheMatiere //TODO Dee ajouter nom de la personne et la classe liéee
        {
            get
            {
                return string.Format("{0}, matière : {1}, temps consacré : {2}, ", id, nom, heure);
            }
        }
        #endregion
    }
}
