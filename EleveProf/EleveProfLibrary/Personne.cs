﻿using System;
namespace EleveProfLibrary
{
    public abstract class Personne
    {
        #region Attributs
        private int id;
        private string nom;
        private string prenom;
        private DateTime dateDeNaissance;
        #endregion

        #region Constructeur
        public Personne(int id, string nom, string prenom, DateTime dateDeNaissance)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
            DateDeNaissance = dateDeNaissance;
        }
        #endregion

        #region Propriétés "classiques"
        public int Id
        {
            get { return id; }
            private set
            {
                if (value < 1)
                    throw new Exception("L'id ne doit pas être inferieur à 1");

                id = value;
            }
        }
        public string Nom
        {
            get { return nom; }
            private set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            private set { prenom = value; }
        }
        public DateTime DateDeNaissance
        {
            get { return dateDeNaissance; }
            private set { dateDeNaissance = value; }
        }

        public virtual string Identite
        {
            get { return string.Format("{0}, {1}, {2}, naissance : {3}", id, nom, prenom, dateDeNaissance); }
        }
        #endregion

    }
}
