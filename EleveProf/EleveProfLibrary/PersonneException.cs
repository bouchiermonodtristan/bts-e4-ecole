﻿using System;
namespace EleveProfLibrary
{
    class PersonneException : Exception
    {
        public PersonneException() : base()
        {
        }

        public PersonneException(string libelle) : base(libelle)
        {
        }

    }

}
