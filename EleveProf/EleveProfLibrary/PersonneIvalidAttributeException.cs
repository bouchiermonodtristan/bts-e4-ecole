﻿using System;
namespace EleveProfLibrary
{
    class PersonneInvalidAttributeException : Exception
    {
        public PersonneInvalidAttributeException()
            : base("Attribut invalide")
        {
        }
        public PersonneInvalidAttributeException(string libelle)
        : base(libelle)
        {
        }

    }

}
