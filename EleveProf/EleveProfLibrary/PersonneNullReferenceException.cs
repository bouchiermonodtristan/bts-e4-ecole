﻿using System;
namespace EleveProfLibrary
{
    class PersonneNullReferenceException : Exception
    {
        public PersonneNullReferenceException()
            : base("La personne est null")
        {
        }
    }
}
