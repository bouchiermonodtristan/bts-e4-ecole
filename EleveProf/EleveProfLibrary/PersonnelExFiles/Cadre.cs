﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;


//namespace PersonnalLibrary
//{
//    public class Cadre : Personne
//    {
//        #region XML
//        internal protected Cadre() : base() { }
//        public override TypePersonneEnum TypePersonne { get { return TypePersonneEnum.Cadre; } }

//        public override string ToXML()
//        {
//            return string.Format("{0}<prime>{1}</prime>", base.ToXML(), Prime);
//        }
//        internal protected override void Deserialize(XmlNode node)
//        {
//            base.Deserialize(node);

//            XmlNode nodeChild = node.SelectSingleNode("prime");
//            if (nodeChild != null)
//            {
//                this.prime = Convert.ToDouble(nodeChild.InnerXml);
//            }
//        }
//        #endregion
//        public enum TypeStatut { Aucun, Technique, Administratif, Juridique, Financier }

//        #region Attributs
//        private TypeStatut status;
//        private double prime;
//        private double taux = 25;
//        #endregion

//        #region Constructeur
//        public Cadre(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrutSaisi,
//            Service serviceSaisi)
//            : base(id, nom, prenom, dateDeNaissance, salaireBrutSaisi, serviceSaisi)
//        {
//        }
//        public Cadre(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrutSaisi,
//            Service serviceSaisi, double prime, double taux, TypeStatut status)
//            : base(id, nom, prenom, dateDeNaissance, salaireBrutSaisi, serviceSaisi)
//        {
//            PRIME = prime;
//            TAUX = taux;
//        }
//        #endregion
//        #region Methodes

//        public TypeStatut Statut
//        {
//            get { return status; }
//        }
//        public double PRIME
//        {
//            get { return prime; }
//            set
//            {
//                if (value > 6000 || value < 0)
//                    throw new Exception("La prime ne peut être supérieur à 6000 et inferieur à 0");

//                prime = value;
//            }
//        }

//        public double TAUX
//        {
//            get { return taux; }
//            set
//            {
//                if (value > 100)
//                    throw new Exception("Le taux ne peut pas être supérieur à 100");

//                taux = value;
//            }
//        }
//        public override string Identite
//        {
//            get { return string.Format("Cadre : {0}, prime {1} salaire net : {2}", base.Identite, PRIME, SalaireNet); }
//        }
//        #endregion

//        public override double SalaireNet => ((base.SalaireNet + PRIME));
//    }
//}
