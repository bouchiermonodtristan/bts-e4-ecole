﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;

//namespace PersonnalLibrary
//{
//    public class Employe : Personne
//    {
//        #region XML
//        internal protected Employe() : base() { }
//        public override TypePersonneEnum TypePersonne { get { return TypePersonneEnum.Employe; } }

//        public override string ToXML()
//        {
//            return string.Format("{0}<rtt>{1}</rtt>", base.ToXML(), RTT);
//        }
//        internal protected override void Deserialize(XmlNode node)
//        {
//            base.Deserialize(node);

//            XmlNode nodeChild = node.SelectSingleNode("rtt");
//            if (nodeChild != null)
//            {
//                this.RTT = Convert.ToInt32(nodeChild.InnerXml);
//            }
//        }
//        #endregion
//        #region Attributs
//        private int nbrtt = 0;
//        private double taux = 20;
//        #endregion

//        #region Constructeur
//        public Employe(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrutSaisi,
//            Service serviceSaisi) 
//            : base(id, nom, prenom, dateDeNaissance, salaireBrutSaisi, serviceSaisi)
//        {
//        }
//        public Employe(int id, string nom, string prenom, DateTime dateDeNaissance, double salaireBrutSaisi,
//            Service serviceSaisi, int rtt, double taux)
//            : base(id, nom, prenom, dateDeNaissance, salaireBrutSaisi, serviceSaisi)
//        {
//            RTT = rtt;
//            TAUX = taux;
//        }
//        #endregion
//        #region Methodes
//        public int RTT
//        {
//            get { return nbrtt; }
//            set
//            {

//                if (value > 26 || value < 0)
//                {
//                    throw new Exception("Le rtt ne peut pas être superieur à 26 jours et inferieur à 0");
//                }

//                nbrtt = value;
//            }
//        }

//        public double TAUX
//        {
//            get { return taux; }
//            set
//            {
//                if (value > 100)
//                    throw new Exception("Le taux ne peut pas être supérieur à 100");

//                taux = value;
//            }
//        }
//        public override string Identite
//        {
//            get { return string.Format("Employe : {0}, rtt {1} salaire net : {2}", base.Identite, RTT, SalaireNet); }
//        }
//        #endregion
//        public override double SalaireNet => (base.SalaireNet * ((100 - TAUX)/100));
//    }
//}
