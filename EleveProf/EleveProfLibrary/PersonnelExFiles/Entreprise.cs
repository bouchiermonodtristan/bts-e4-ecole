﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace PersonnalLibrary
//{
//    public class Entreprise
//    {
//        public List<Service> listeServices = new List<Service>();
//        public List<Personne> listePersonnes = new List<Personne>();

//        public List<Personne> Personnes
//        {
//            get { return new List<Personne>(listePersonnes); }
//        }
//        public List<Service> Services
//        {
//            get { return new List<Service>(listeServices); }
//        }
//        public bool AjouterService(Service service)
//        {
//            if (service == null)
//                throw new Exception();

//            this.listeServices.Add(service);
//            return true;
//        }
//        public virtual void ChargerPersonnes() { }
//        public virtual void SauvegarderPersonnes() { }
//        public bool AjouterPersonne(Personne personne)
//        {
//            if (personne == null)
//                new PersonneNullReferenceException();

//            this.listePersonnes.Add(personne);
//                return true;
//        }
//        public bool ModifierPersonne(Personne personne)
//        {
//            if (personne == null)
//                new PersonneNullReferenceException();

//            Personne personneAModifier = RechercherPersonne(personne.Id);
//            if (personneAModifier == null)
//                new PersonneNullReferenceException(); ; //La personne n'existe pas

//            //on fait les modifs (on peut uniquement sur les attributs publics de personne)
//            personneAModifier.Service = personne.Service;
//            personneAModifier.SalaireBrut = personne.SalaireBrut;
//            return true;
//        }
//        public bool SupprimerPersonne(Personne personne)
//        {
//            if (personne == null)
//                new PersonneNullReferenceException();

//            listePersonnes.Remove(personne);
//            return true;
//        }
//        public bool SupprimerPersonne(int id)
//        {
//            listePersonnes.Remove(RechercherPersonne(id));
//            return true;
//        }
//        Personne RechercherPersonne(int id)
//        {
//            if (listePersonnes.Find(personne => personne.Id == id) == null)
//                new PersonneNullReferenceException();

//            return listePersonnes.Find(personne => personne.Id == id);
//        }
//        public Service RechercherService(int id)
//        {
//            if (listeServices.Find(service => service.Id == id) == null)
//                throw new Exception();

//            return listeServices.Find(service => service.Id == id);
//        }
//        List<Personne> RechercherPersonnesCommencePar(string nom)
//        {
//            return listePersonnes.FindAll(delegate (Personne personne)
//            {
//                return personne.Nom.StartsWith(nom, StringComparison.InvariantCultureIgnoreCase);
//            });
//        }
//        public string AfficherPersonne()
//        {
//            for (int i = 0; i < listePersonnes.Count; i++)
//            {
//                Console.WriteLine(string.Format("{0}", listePersonnes[i].Identite)); 
//            }
//            return "-1";
//        }

//        //Vous devez écrire une méthode qui recherche toutes les personnes pour un service donné.
//    }
//}
