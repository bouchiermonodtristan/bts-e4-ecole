﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;

//namespace PersonnalLibrary
//{
//    public class EntrepriseXML : Entreprise
//    {
//        // Chemin vers le fichier XML
//        // Exemple : C:\data\
//        public string FilePathName { get; set; }

//        // Chargement des données XML
//        public override void ChargerPersonnes()
//        {
//            try
//            {
//                XmlDocument doc = new XmlDocument();
//                // Chargement du fichier XML
//                doc.Load(Path.Combine(FilePathName, "personnes.xml"));

//                // Vérification de la racine
//                XmlNode rootNode = doc.DocumentElement;
//                if (rootNode != null && rootNode.HasChildNodes)
//                {
//                    // racine == personnes
//                    if (!rootNode.Name.Equals("entreprise"))
//                        throw new Exception("La racine doit être égale à <personnes>");

//                    // Parcours des noeuds services
//                    XmlNode nodeChild = rootNode.SelectSingleNode("services").FirstChild;
//                    while (nodeChild != null)
//                    {
//                        // Appel de la méthode de lecture
//                        LoadDataService(nodeChild);
//                        nodeChild = nodeChild.NextSibling;
//                    }
//                    // Parcours des noeuds services
//                    nodeChild = rootNode.SelectSingleNode("personnes").FirstChild;
//                    while (nodeChild != null)
//                    {
//                        // Appel de la méthode de lecture
//                        LoadDataPersonne(nodeChild);
//                        nodeChild = nodeChild.NextSibling;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("La personne n'a pas été chargée, erreur : " + ex);
//            }
//        }

//        // Chargement des services
//        private void LoadDataService(XmlNode node)
//        {
//            // Vérification du type d'éléments
//            if (node.NodeType == XmlNodeType.Element)
//            {
//                try
//                {
//                    Service service = new Service();
//                    // Appel de la méthode virtuelle
//                    service.Deserialize(node);

//                    // Ajout de la personne dans la liste
//                    this.AjouterService(service);
//                }
//                catch (Exception ex)
//                {
//                    throw new Exception("Le service n'a pas été chargé, erreur : "+ex);
//                }
//            }
//        }

//        // Chargement des personnes
//        private void LoadDataPersonne(XmlNode node)
//        {
//            // Vérification du type d'éléments
//            if (node.NodeType == XmlNodeType.Element)
//            {
//                try
//                {
//                    Personne personne = null;
//                    // Obtention du statut au format string
//                    string statutAsString = node.SelectSingleNode("statut").InnerXml;
//                    // Conversion du statut au format string en énumération
//                    Personne.TypePersonneEnum typePersonne = (Personne.TypePersonneEnum)Enum.Parse(typeof(Personne.TypePersonneEnum), statutAsString);
//                    // Création de l'objet d'après son statut
//                    switch (typePersonne)
//                    {
//                        case Personne.TypePersonneEnum.Employe:
//                            // Le constructeur a pour type d'accès "internal" donc il peut être appelé 
//                            // dans la librairie mais pas à l'extérieur de la librairie
//                            // https://msdn.microsoft.com/fr-fr/library/ba0a1yw2.aspx 
//                            personne = new Employe();
//                            break;
//                        case Personne.TypePersonneEnum.Cadre:
//                            personne = new Cadre();
//                            break;
//                        default:
//                            throw new Exception("Le noeud n’est pas reconnu");
//                    }
//                    // Appel de la méthode virtuelle
//                    personne.Deserialize(node);

//                    // Obtention du service créé à partir du XML
//                    if (personne.Service != null)
//                    {
//                        Service service = RechercherService(personne.Service.Id);
//                        if (service != null)
//                        {
//                            // Si le service existe, on remplace le service construit dans le XML par le service existant dans l'entreprise
//                            personne.Service = service;
//                        }
//                        else
//                        {
//                            // Sinon on l'ajoute à la liste des services
//                            this.AjouterService(personne.Service);
//                        }
//                    }

//                    // Ajout de la personne dans la liste
//                    this.AjouterPersonne(personne);
//                }
//                catch (Exception ex)
//                {
//                    throw new Exception("Les données de la personne n'ont pas été chargées, erreur : " + ex);
//                }

//            }
//        }

//        // Entregistrement dans un fichier XML
//        public override void SauvegarderPersonnes()
//        {
//            // L'enregistrment se fait comme un fichier texte
//            try
//            {
//                // Le mot clé using permet de libérer automatiquement la ressource
//                // https://msdn.microsoft.com/fr-fr/library/yh598w02.aspx 
//                using (StreamWriter sw = new StreamWriter(Path.Combine(FilePathName, "personnes.xml")))
//                {
//                    sw.WriteLine("<entreprise>");

//                    sw.WriteLine("<services>");
//                    foreach (Service s in Services)
//                    {
//                        sw.WriteLine("<service>");
//                        sw.WriteLine(s.ToXML());
//                        sw.WriteLine("</service>");
//                    }
//                    sw.WriteLine("</services>");

//                    sw.WriteLine("<personnes>");
//                    foreach (Personne p in Personnes)
//                    {
//                        sw.WriteLine("<personne>");
//                        sw.WriteLine(p.ToXML());
//                        sw.WriteLine("</personne>");
//                    }
//                    sw.WriteLine("</personnes>");

//                    sw.WriteLine("</entreprise>");
//                }

//            }
//            catch (Exception ex)
//            {
//                throw new Exception("La personne n'a pas pu être sauvegardée, erreur : " + ex);
//            }
//        }
//    }
//}
