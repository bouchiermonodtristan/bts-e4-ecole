﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonnalLibrary
{
    class PersonneException : Exception
    {
        public PersonneException() : base()
        {
        }

        public PersonneException(string libelle) : base(libelle)
        {
        }

    }
}
