﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonnalLibrary
{
    class PersonneInvalidAttributeException : Exception
    {
        public PersonneInvalidAttributeException()
            : base("Attribut invalide")
        {
        }
        public PersonneInvalidAttributeException(string libelle)
        : base(libelle)
        {
        }

    }
}
