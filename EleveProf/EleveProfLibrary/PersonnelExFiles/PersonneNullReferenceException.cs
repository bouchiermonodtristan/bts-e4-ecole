﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonnalLibrary
{
    class PersonneNullReferenceException : Exception
    {
        public PersonneNullReferenceException()
            : base("La personne est null")
        {
        }
    }
}
