﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Xml;


//namespace PersonnalLibrary
//{
//    public class Service
//    {
//        #region XML
//        internal protected Service() { }
//        public virtual string ToXML()
//        {
//            return string.Format("<id>{0}</id><libelle>{1}</libelle>",
//                                    Id, Libelle);
//        }
//        internal protected virtual void Deserialize(XmlNode node)
//        {
//            if (!node.HasChildNodes)
//                throw new Exception("Le service n'a aucune propriété");

//            XmlNode nodeChild = node.FirstChild;
//            while (nodeChild != null)
//            {
//                switch (nodeChild.Name.ToLower())
//                {
//                    case "id":
//                        this.Id = Convert.ToInt32(nodeChild.InnerXml);
//                        break;
//                    case "libelle":
//                        this.Libelle = nodeChild.InnerXml;
//                        break;
//                }
//                nodeChild = nodeChild.NextSibling;
//            }
//        }
//        #endregion

//        private string libelle;
//        private int id;
//        public Service(int id, string libelle)
//        {
//            Id = id;
//            Libelle = libelle;
//        }
//        public int Id
//        {
//            get { return id; }
//            set { id = value; }
//        }
//        public string Libelle
//        {
//            get { return libelle; }
//            set { libelle = value; }
//        }
//    }
//}
