﻿using System;
namespace EleveProfLibrary
{
    public class Prof : Personne
    {
        public enum TypeStatut { 
            Intervenant = 0, 
            Permanent = 1
        }

        #region Attributs
        private TypeStatut statut;
        private float salaire;
        #endregion

        #region Getters/setters
        public TypeStatut Statut
        {
            get => statut;
            set => statut = value;
        }
        public float Salaire
        {
            get => salaire;
            set
            {
                if (value <= 0)
                {
                    throw new Exception("Le salaire ne peut être négatif ou nul");
                }
                else
                {
                    salaire = value;
                }
            }
        }

        #endregion

        #region Constructeur
        public Prof(int id, string nom, string prenom, DateTime dateDeNaissance, TypeStatut statut, float salaire)
            : base(id, nom, prenom, dateDeNaissance)
        {
            Statut = statut;
            Salaire = salaire;
        }
        public Prof(int id, string nom, string prenom, DateTime dateDeNaissance, float salaire)
            : base(id, nom, prenom, dateDeNaissance)
        {
            Salaire = salaire;
        }
        #endregion

        #region Constructeurs et Methodes

        public override string Identite
        {
            get { return string.Format("Prof : {0}, statut {1}, salaire : {2}", base.Identite, statut, salaire); }
        }

        #endregion
    }
}
