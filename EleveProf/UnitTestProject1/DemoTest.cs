﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

[TestClass]
public class DemoTest
{/*
    static Commande cmd = null;
    static Produit table = null;

    [ClassInitialize]
    public static void InitialisationTests(TestContext context)
    {
        // Exemple d'initialisation
        table = new Produit(1, "Table", 130, 6);
        cmd = new Commande(1, DateTime.Now);
    }

    [TestMethod]
    public void TestAjoutLigneCommande()
    {
        // On ajoute la table
        cmd.AjouterLigneCommande(table, 1);
        // Le produit table doit forcément être dans la commande
        LigneCommande lc = cmd.RechercherLigneCommandeProduit(table.Id);
        Assert.IsNotNull(lc);
        Assert.AreEqual(lc.Produit.Id, table.Id);
    }

    [TestMethod]
    public void TestModificationLigneCommande()
    {
        Produit chaise = new Produit(2, "Chaise", 50, 51);
        // On ajoute la chaise
        cmd.AjouterLigneCommande(chaise, 4);
        cmd.AjouterLigneCommande(chaise, 1);
        // Le produit chaise doit avoir la quantité égale à 5
        LigneCommande lc = cmd.RechercherLigneCommandeProduit(chaise.Id);
        Assert.IsNotNull(lc);
        Assert.AreEqual(lc.Quantite, 5);
    }

    [TestMethod]
    public void TestRAZ()
    {
        // On vide la commande
        cmd.ViderCommande();
        Assert.AreEqual(cmd.NbLignesCommande, 0);
    }

    [TestMethod]
    public void TestPrixTotal()
    {
        Produit chaise = new Produit(2, "Chaise", 50, 51);
        Commande newCmd = new Commande(1, DateTime.Now);
        // On ajoute la chaise
        newCmd.AjouterLigneCommande(chaise, 4); // 4 * 50 euros
        newCmd.AjouterLigneCommande(chaise, 1); // 1 * 50
        table.PrixHT = 150;
        newCmd.AjouterLigneCommande(table, 1);  // 1 * 150
        // Le total HT doit être égal à 400
        Assert.AreEqual(newCmd.TotalHT, 400);
    }

    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void TestAjoutAvecException()
    {
        // L'ajout d'un produit avec une quantité négative doit déclencher une exception
        new LigneCommande(table, -1);
    }*/

}
