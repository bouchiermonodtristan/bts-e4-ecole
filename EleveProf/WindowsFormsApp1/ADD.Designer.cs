﻿namespace WindowsFormsApp1
{
    partial class ADD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroTileMatiere = new MetroFramework.Controls.MetroTile();
            this.metroTileEleve = new MetroFramework.Controls.MetroTile();
            this.metroTileProf = new MetroFramework.Controls.MetroTile();
            this.metroTileClasse = new MetroFramework.Controls.MetroTile();
            this.metroLabelClasseSigle = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxClasseSigle = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonConfirmAddClasse = new MetroFramework.Controls.MetroButton();
            this.metroLabelProfNom = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxProfNom = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelProfPrenom = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxProfPrenom = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelDateNaiss = new MetroFramework.Controls.MetroLabel();
            this.metroDateTimeProf = new MetroFramework.Controls.MetroDateTime();
            this.metroLabelProfStatut = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxProf = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelSalaireProf = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxSalaireProf = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonAddProf = new MetroFramework.Controls.MetroButton();
            this.metroLabelMatiereNom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelMatiereHeure = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxMatiereNom = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxMatiereHeure = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonAddMatiere = new MetroFramework.Controls.MetroButton();
            this.metroLabelEleveNom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelElevePrenom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveDatNaiss = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveFormation = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveMoyenne = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxEleveNom = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxElevePrenom = new MetroFramework.Controls.MetroTextBox();
            this.metroDateTimeEleveDateNaiss = new MetroFramework.Controls.MetroDateTime();
            this.metroComboBoxEleveFormation = new MetroFramework.Controls.MetroComboBox();
            this.metroTextBoxEleveMoyenne = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonEleveAdd = new MetroFramework.Controls.MetroButton();
            this.metroLabelEleveClasse = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxEleveClasse = new MetroFramework.Controls.MetroComboBox();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroComboBoxMatiereClasse = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBoxMatiereProf = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelMatiereClasse = new MetroFramework.Controls.MetroLabel();
            this.metroLabelMatiereProf = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(398, 62);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(119, 37);
            this.metroButton4.TabIndex = 9;
            this.metroButton4.Text = "Delete";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(273, 62);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(119, 37);
            this.metroButton3.TabIndex = 8;
            this.metroButton3.Text = "Update";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(148, 63);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(119, 37);
            this.metroButton2.TabIndex = 7;
            this.metroButton2.Text = "Add";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(23, 63);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(119, 37);
            this.metroButton1.TabIndex = 6;
            this.metroButton1.Text = "View";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroTileMatiere
            // 
            this.metroTileMatiere.ActiveControl = null;
            this.metroTileMatiere.Location = new System.Drawing.Point(765, 63);
            this.metroTileMatiere.Name = "metroTileMatiere";
            this.metroTileMatiere.Size = new System.Drawing.Size(150, 44);
            this.metroTileMatiere.TabIndex = 13;
            this.metroTileMatiere.Text = "Generate Matiere";
            this.metroTileMatiere.UseSelectable = true;
            this.metroTileMatiere.Click += new System.EventHandler(this.metroTileMatiere_Click);
            // 
            // metroTileEleve
            // 
            this.metroTileEleve.ActiveControl = null;
            this.metroTileEleve.Location = new System.Drawing.Point(764, 113);
            this.metroTileEleve.Name = "metroTileEleve";
            this.metroTileEleve.Size = new System.Drawing.Size(151, 45);
            this.metroTileEleve.TabIndex = 12;
            this.metroTileEleve.Text = "Generate Eleve";
            this.metroTileEleve.UseSelectable = true;
            this.metroTileEleve.Click += new System.EventHandler(this.metroTileEleve_Click);
            // 
            // metroTileProf
            // 
            this.metroTileProf.ActiveControl = null;
            this.metroTileProf.Location = new System.Drawing.Point(607, 113);
            this.metroTileProf.Name = "metroTileProf";
            this.metroTileProf.Size = new System.Drawing.Size(151, 45);
            this.metroTileProf.TabIndex = 10;
            this.metroTileProf.Text = "Generate Prof";
            this.metroTileProf.UseSelectable = true;
            this.metroTileProf.Click += new System.EventHandler(this.metroTileProf_Click);
            // 
            // metroTileClasse
            // 
            this.metroTileClasse.ActiveControl = null;
            this.metroTileClasse.Location = new System.Drawing.Point(607, 63);
            this.metroTileClasse.Name = "metroTileClasse";
            this.metroTileClasse.Size = new System.Drawing.Size(151, 44);
            this.metroTileClasse.TabIndex = 11;
            this.metroTileClasse.Text = "Generate Classe";
            this.metroTileClasse.UseSelectable = true;
            this.metroTileClasse.Click += new System.EventHandler(this.metroTileClasse_Click);
            // 
            // metroLabelClasseSigle
            // 
            this.metroLabelClasseSigle.AutoSize = true;
            this.metroLabelClasseSigle.Location = new System.Drawing.Point(41, 132);
            this.metroLabelClasseSigle.Name = "metroLabelClasseSigle";
            this.metroLabelClasseSigle.Size = new System.Drawing.Size(81, 20);
            this.metroLabelClasseSigle.TabIndex = 21;
            this.metroLabelClasseSigle.Text = "Classe Sigle";
            this.metroLabelClasseSigle.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelClasseSigle.Click += new System.EventHandler(this.metroLabelClasseSigle_Click);
            // 
            // metroTextBoxClasseSigle
            // 
            // 
            // 
            // 
            this.metroTextBoxClasseSigle.CustomButton.Image = null;
            this.metroTextBoxClasseSigle.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxClasseSigle.CustomButton.Name = "";
            this.metroTextBoxClasseSigle.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxClasseSigle.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClasseSigle.CustomButton.TabIndex = 1;
            this.metroTextBoxClasseSigle.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClasseSigle.CustomButton.UseSelectable = true;
            this.metroTextBoxClasseSigle.CustomButton.Visible = false;
            this.metroTextBoxClasseSigle.Lines = new string[0];
            this.metroTextBoxClasseSigle.Location = new System.Drawing.Point(148, 135);
            this.metroTextBoxClasseSigle.MaxLength = 32767;
            this.metroTextBoxClasseSigle.Name = "metroTextBoxClasseSigle";
            this.metroTextBoxClasseSigle.PasswordChar = '\0';
            this.metroTextBoxClasseSigle.PromptText = "Insert Classe Sigle....";
            this.metroTextBoxClasseSigle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClasseSigle.SelectedText = "";
            this.metroTextBoxClasseSigle.SelectionLength = 0;
            this.metroTextBoxClasseSigle.SelectionStart = 0;
            this.metroTextBoxClasseSigle.ShortcutsEnabled = true;
            this.metroTextBoxClasseSigle.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxClasseSigle.TabIndex = 20;
            this.metroTextBoxClasseSigle.UseSelectable = true;
            this.metroTextBoxClasseSigle.WaterMark = "Insert Classe Sigle....";
            this.metroTextBoxClasseSigle.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClasseSigle.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxClasseSigle.Click += new System.EventHandler(this.metroTextBoxClasseSigle_Click);
            // 
            // metroButtonConfirmAddClasse
            // 
            this.metroButtonConfirmAddClasse.Location = new System.Drawing.Point(273, 181);
            this.metroButtonConfirmAddClasse.Name = "metroButtonConfirmAddClasse";
            this.metroButtonConfirmAddClasse.Size = new System.Drawing.Size(119, 23);
            this.metroButtonConfirmAddClasse.TabIndex = 22;
            this.metroButtonConfirmAddClasse.Text = "Confirm Add";
            this.metroButtonConfirmAddClasse.UseSelectable = true;
            this.metroButtonConfirmAddClasse.Click += new System.EventHandler(this.metroButtonConfirmAddClasse_Click);
            // 
            // metroLabelProfNom
            // 
            this.metroLabelProfNom.AutoSize = true;
            this.metroLabelProfNom.Location = new System.Drawing.Point(41, 132);
            this.metroLabelProfNom.Name = "metroLabelProfNom";
            this.metroLabelProfNom.Size = new System.Drawing.Size(69, 20);
            this.metroLabelProfNom.TabIndex = 24;
            this.metroLabelProfNom.Text = "Prof Nom";
            this.metroLabelProfNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelProfNom.Click += new System.EventHandler(this.metroLabelProfNom_Click);
            // 
            // metroTextBoxProfNom
            // 
            // 
            // 
            // 
            this.metroTextBoxProfNom.CustomButton.Image = null;
            this.metroTextBoxProfNom.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxProfNom.CustomButton.Name = "";
            this.metroTextBoxProfNom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxProfNom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxProfNom.CustomButton.TabIndex = 1;
            this.metroTextBoxProfNom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfNom.CustomButton.UseSelectable = true;
            this.metroTextBoxProfNom.CustomButton.Visible = false;
            this.metroTextBoxProfNom.Lines = new string[0];
            this.metroTextBoxProfNom.Location = new System.Drawing.Point(148, 135);
            this.metroTextBoxProfNom.MaxLength = 32767;
            this.metroTextBoxProfNom.Name = "metroTextBoxProfNom";
            this.metroTextBoxProfNom.PasswordChar = '\0';
            this.metroTextBoxProfNom.PromptText = "Insert Prof Nom....";
            this.metroTextBoxProfNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxProfNom.SelectedText = "";
            this.metroTextBoxProfNom.SelectionLength = 0;
            this.metroTextBoxProfNom.SelectionStart = 0;
            this.metroTextBoxProfNom.ShortcutsEnabled = true;
            this.metroTextBoxProfNom.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxProfNom.TabIndex = 23;
            this.metroTextBoxProfNom.UseSelectable = true;
            this.metroTextBoxProfNom.WaterMark = "Insert Prof Nom....";
            this.metroTextBoxProfNom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxProfNom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxProfNom.Click += new System.EventHandler(this.metroTextBoxProfNom_Click);
            // 
            // metroLabelProfPrenom
            // 
            this.metroLabelProfPrenom.AutoSize = true;
            this.metroLabelProfPrenom.Location = new System.Drawing.Point(41, 178);
            this.metroLabelProfPrenom.Name = "metroLabelProfPrenom";
            this.metroLabelProfPrenom.Size = new System.Drawing.Size(58, 20);
            this.metroLabelProfPrenom.TabIndex = 26;
            this.metroLabelProfPrenom.Text = "Prenom";
            this.metroLabelProfPrenom.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelProfPrenom.Click += new System.EventHandler(this.metroLabelProfPrenom_Click);
            // 
            // metroTextBoxProfPrenom
            // 
            // 
            // 
            // 
            this.metroTextBoxProfPrenom.CustomButton.Image = null;
            this.metroTextBoxProfPrenom.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxProfPrenom.CustomButton.Name = "";
            this.metroTextBoxProfPrenom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxProfPrenom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxProfPrenom.CustomButton.TabIndex = 1;
            this.metroTextBoxProfPrenom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfPrenom.CustomButton.UseSelectable = true;
            this.metroTextBoxProfPrenom.CustomButton.Visible = false;
            this.metroTextBoxProfPrenom.Lines = new string[0];
            this.metroTextBoxProfPrenom.Location = new System.Drawing.Point(148, 181);
            this.metroTextBoxProfPrenom.MaxLength = 32767;
            this.metroTextBoxProfPrenom.Name = "metroTextBoxProfPrenom";
            this.metroTextBoxProfPrenom.PasswordChar = '\0';
            this.metroTextBoxProfPrenom.PromptText = "Insert Prof Prenom....";
            this.metroTextBoxProfPrenom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxProfPrenom.SelectedText = "";
            this.metroTextBoxProfPrenom.SelectionLength = 0;
            this.metroTextBoxProfPrenom.SelectionStart = 0;
            this.metroTextBoxProfPrenom.ShortcutsEnabled = true;
            this.metroTextBoxProfPrenom.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxProfPrenom.TabIndex = 25;
            this.metroTextBoxProfPrenom.UseSelectable = true;
            this.metroTextBoxProfPrenom.WaterMark = "Insert Prof Prenom....";
            this.metroTextBoxProfPrenom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxProfPrenom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxProfPrenom.Click += new System.EventHandler(this.metroTextBoxProfPrenom_Click);
            // 
            // metroLabelDateNaiss
            // 
            this.metroLabelDateNaiss.AutoSize = true;
            this.metroLabelDateNaiss.Location = new System.Drawing.Point(41, 224);
            this.metroLabelDateNaiss.Name = "metroLabelDateNaiss";
            this.metroLabelDateNaiss.Size = new System.Drawing.Size(75, 20);
            this.metroLabelDateNaiss.TabIndex = 28;
            this.metroLabelDateNaiss.Text = "Date Naiss";
            this.metroLabelDateNaiss.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelDateNaiss.Click += new System.EventHandler(this.metroLabelDateNaiss_Click);
            // 
            // metroDateTimeProf
            // 
            this.metroDateTimeProf.Location = new System.Drawing.Point(148, 221);
            this.metroDateTimeProf.MinimumSize = new System.Drawing.Size(4, 30);
            this.metroDateTimeProf.Name = "metroDateTimeProf";
            this.metroDateTimeProf.Size = new System.Drawing.Size(244, 30);
            this.metroDateTimeProf.TabIndex = 29;
            this.metroDateTimeProf.ValueChanged += new System.EventHandler(this.metroDateTimeProf_ValueChanged);
            // 
            // metroLabelProfStatut
            // 
            this.metroLabelProfStatut.AutoSize = true;
            this.metroLabelProfStatut.Location = new System.Drawing.Point(41, 268);
            this.metroLabelProfStatut.Name = "metroLabelProfStatut";
            this.metroLabelProfStatut.Size = new System.Drawing.Size(43, 20);
            this.metroLabelProfStatut.TabIndex = 31;
            this.metroLabelProfStatut.Text = "Statut";
            this.metroLabelProfStatut.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelProfStatut.Click += new System.EventHandler(this.metroLabelProfStatut_Click);
            // 
            // metroComboBoxProf
            // 
            this.metroComboBoxProf.AutoCompleteCustomSource.AddRange(new string[] {
            "Aucun",
            "Intervenant",
            "Permanent"});
            this.metroComboBoxProf.FormattingEnabled = true;
            this.metroComboBoxProf.ItemHeight = 24;
            this.metroComboBoxProf.Items.AddRange(new object[] {
            "Aucun",
            "Intervenant",
            "Permanent"});
            this.metroComboBoxProf.Location = new System.Drawing.Point(148, 268);
            this.metroComboBoxProf.Name = "metroComboBoxProf";
            this.metroComboBoxProf.PromptText = "Aucun";
            this.metroComboBoxProf.Size = new System.Drawing.Size(244, 30);
            this.metroComboBoxProf.TabIndex = 32;
            this.metroComboBoxProf.UseSelectable = true;
            this.metroComboBoxProf.SelectedIndexChanged += new System.EventHandler(this.metroComboBoxProf_SelectedIndexChanged);
            // 
            // metroLabelSalaireProf
            // 
            this.metroLabelSalaireProf.AutoSize = true;
            this.metroLabelSalaireProf.Location = new System.Drawing.Point(41, 315);
            this.metroLabelSalaireProf.Name = "metroLabelSalaireProf";
            this.metroLabelSalaireProf.Size = new System.Drawing.Size(49, 20);
            this.metroLabelSalaireProf.TabIndex = 34;
            this.metroLabelSalaireProf.Text = "Salaire";
            this.metroLabelSalaireProf.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelSalaireProf.Click += new System.EventHandler(this.metroLabelSalaireProf_Click);
            // 
            // metroTextBoxSalaireProf
            // 
            // 
            // 
            // 
            this.metroTextBoxSalaireProf.CustomButton.Image = null;
            this.metroTextBoxSalaireProf.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxSalaireProf.CustomButton.Name = "";
            this.metroTextBoxSalaireProf.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxSalaireProf.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxSalaireProf.CustomButton.TabIndex = 1;
            this.metroTextBoxSalaireProf.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxSalaireProf.CustomButton.UseSelectable = true;
            this.metroTextBoxSalaireProf.CustomButton.Visible = false;
            this.metroTextBoxSalaireProf.Lines = new string[0];
            this.metroTextBoxSalaireProf.Location = new System.Drawing.Point(148, 318);
            this.metroTextBoxSalaireProf.MaxLength = 32767;
            this.metroTextBoxSalaireProf.Name = "metroTextBoxSalaireProf";
            this.metroTextBoxSalaireProf.PasswordChar = '\0';
            this.metroTextBoxSalaireProf.PromptText = "Insert Prof Salaire....";
            this.metroTextBoxSalaireProf.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxSalaireProf.SelectedText = "";
            this.metroTextBoxSalaireProf.SelectionLength = 0;
            this.metroTextBoxSalaireProf.SelectionStart = 0;
            this.metroTextBoxSalaireProf.ShortcutsEnabled = true;
            this.metroTextBoxSalaireProf.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxSalaireProf.TabIndex = 33;
            this.metroTextBoxSalaireProf.UseSelectable = true;
            this.metroTextBoxSalaireProf.WaterMark = "Insert Prof Salaire....";
            this.metroTextBoxSalaireProf.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxSalaireProf.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxSalaireProf.Click += new System.EventHandler(this.metroTextBoxSalaireProf_Click);
            // 
            // metroButtonAddProf
            // 
            this.metroButtonAddProf.Location = new System.Drawing.Point(273, 360);
            this.metroButtonAddProf.Name = "metroButtonAddProf";
            this.metroButtonAddProf.Size = new System.Drawing.Size(118, 30);
            this.metroButtonAddProf.TabIndex = 35;
            this.metroButtonAddProf.Text = "Confirmer Add";
            this.metroButtonAddProf.UseSelectable = true;
            this.metroButtonAddProf.Click += new System.EventHandler(this.metroButtonAddProf_Click);
            // 
            // metroLabelMatiereNom
            // 
            this.metroLabelMatiereNom.AutoSize = true;
            this.metroLabelMatiereNom.Location = new System.Drawing.Point(41, 132);
            this.metroLabelMatiereNom.Name = "metroLabelMatiereNom";
            this.metroLabelMatiereNom.Size = new System.Drawing.Size(40, 20);
            this.metroLabelMatiereNom.TabIndex = 36;
            this.metroLabelMatiereNom.Text = "Nom";
            this.metroLabelMatiereNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelMatiereHeure
            // 
            this.metroLabelMatiereHeure.AutoSize = true;
            this.metroLabelMatiereHeure.Location = new System.Drawing.Point(41, 178);
            this.metroLabelMatiereHeure.Name = "metroLabelMatiereHeure";
            this.metroLabelMatiereHeure.Size = new System.Drawing.Size(81, 20);
            this.metroLabelMatiereHeure.TabIndex = 37;
            this.metroLabelMatiereHeure.Text = "Heures (int)";
            this.metroLabelMatiereHeure.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxMatiereNom
            // 
            // 
            // 
            // 
            this.metroTextBoxMatiereNom.CustomButton.Image = null;
            this.metroTextBoxMatiereNom.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxMatiereNom.CustomButton.Name = "";
            this.metroTextBoxMatiereNom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxMatiereNom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxMatiereNom.CustomButton.TabIndex = 1;
            this.metroTextBoxMatiereNom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereNom.CustomButton.UseSelectable = true;
            this.metroTextBoxMatiereNom.CustomButton.Visible = false;
            this.metroTextBoxMatiereNom.Lines = new string[0];
            this.metroTextBoxMatiereNom.Location = new System.Drawing.Point(148, 135);
            this.metroTextBoxMatiereNom.MaxLength = 32767;
            this.metroTextBoxMatiereNom.Name = "metroTextBoxMatiereNom";
            this.metroTextBoxMatiereNom.PasswordChar = '\0';
            this.metroTextBoxMatiereNom.PromptText = "Insert Matiere Nom....";
            this.metroTextBoxMatiereNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxMatiereNom.SelectedText = "";
            this.metroTextBoxMatiereNom.SelectionLength = 0;
            this.metroTextBoxMatiereNom.SelectionStart = 0;
            this.metroTextBoxMatiereNom.ShortcutsEnabled = true;
            this.metroTextBoxMatiereNom.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxMatiereNom.TabIndex = 38;
            this.metroTextBoxMatiereNom.UseSelectable = true;
            this.metroTextBoxMatiereNom.WaterMark = "Insert Matiere Nom....";
            this.metroTextBoxMatiereNom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxMatiereNom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxMatiereHeure
            // 
            // 
            // 
            // 
            this.metroTextBoxMatiereHeure.CustomButton.Image = null;
            this.metroTextBoxMatiereHeure.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxMatiereHeure.CustomButton.Name = "";
            this.metroTextBoxMatiereHeure.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxMatiereHeure.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxMatiereHeure.CustomButton.TabIndex = 1;
            this.metroTextBoxMatiereHeure.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereHeure.CustomButton.UseSelectable = true;
            this.metroTextBoxMatiereHeure.CustomButton.Visible = false;
            this.metroTextBoxMatiereHeure.Lines = new string[0];
            this.metroTextBoxMatiereHeure.Location = new System.Drawing.Point(147, 181);
            this.metroTextBoxMatiereHeure.MaxLength = 32767;
            this.metroTextBoxMatiereHeure.Name = "metroTextBoxMatiereHeure";
            this.metroTextBoxMatiereHeure.PasswordChar = '\0';
            this.metroTextBoxMatiereHeure.PromptText = "Insert Nbr Heures....";
            this.metroTextBoxMatiereHeure.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxMatiereHeure.SelectedText = "";
            this.metroTextBoxMatiereHeure.SelectionLength = 0;
            this.metroTextBoxMatiereHeure.SelectionStart = 0;
            this.metroTextBoxMatiereHeure.ShortcutsEnabled = true;
            this.metroTextBoxMatiereHeure.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxMatiereHeure.TabIndex = 39;
            this.metroTextBoxMatiereHeure.UseSelectable = true;
            this.metroTextBoxMatiereHeure.WaterMark = "Insert Nbr Heures....";
            this.metroTextBoxMatiereHeure.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxMatiereHeure.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonAddMatiere
            // 
            this.metroButtonAddMatiere.Location = new System.Drawing.Point(274, 311);
            this.metroButtonAddMatiere.Name = "metroButtonAddMatiere";
            this.metroButtonAddMatiere.Size = new System.Drawing.Size(118, 30);
            this.metroButtonAddMatiere.TabIndex = 40;
            this.metroButtonAddMatiere.Text = "Confirmer Add";
            this.metroButtonAddMatiere.UseSelectable = true;
            this.metroButtonAddMatiere.Click += new System.EventHandler(this.metroButtonAddMatiere_Click);
            // 
            // metroLabelEleveNom
            // 
            this.metroLabelEleveNom.AutoSize = true;
            this.metroLabelEleveNom.Location = new System.Drawing.Point(41, 132);
            this.metroLabelEleveNom.Name = "metroLabelEleveNom";
            this.metroLabelEleveNom.Size = new System.Drawing.Size(40, 20);
            this.metroLabelEleveNom.TabIndex = 41;
            this.metroLabelEleveNom.Text = "Nom";
            this.metroLabelEleveNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelElevePrenom
            // 
            this.metroLabelElevePrenom.AutoSize = true;
            this.metroLabelElevePrenom.Location = new System.Drawing.Point(41, 178);
            this.metroLabelElevePrenom.Name = "metroLabelElevePrenom";
            this.metroLabelElevePrenom.Size = new System.Drawing.Size(58, 20);
            this.metroLabelElevePrenom.TabIndex = 42;
            this.metroLabelElevePrenom.Text = "Prenom";
            this.metroLabelElevePrenom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveDatNaiss
            // 
            this.metroLabelEleveDatNaiss.AutoSize = true;
            this.metroLabelEleveDatNaiss.Location = new System.Drawing.Point(41, 221);
            this.metroLabelEleveDatNaiss.Name = "metroLabelEleveDatNaiss";
            this.metroLabelEleveDatNaiss.Size = new System.Drawing.Size(75, 20);
            this.metroLabelEleveDatNaiss.TabIndex = 43;
            this.metroLabelEleveDatNaiss.Text = "Date Naiss";
            this.metroLabelEleveDatNaiss.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveFormation
            // 
            this.metroLabelEleveFormation.AutoSize = true;
            this.metroLabelEleveFormation.Location = new System.Drawing.Point(41, 268);
            this.metroLabelEleveFormation.Name = "metroLabelEleveFormation";
            this.metroLabelEleveFormation.Size = new System.Drawing.Size(71, 20);
            this.metroLabelEleveFormation.TabIndex = 44;
            this.metroLabelEleveFormation.Text = "Formation";
            this.metroLabelEleveFormation.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveMoyenne
            // 
            this.metroLabelEleveMoyenne.AutoSize = true;
            this.metroLabelEleveMoyenne.Location = new System.Drawing.Point(41, 315);
            this.metroLabelEleveMoyenne.Name = "metroLabelEleveMoyenne";
            this.metroLabelEleveMoyenne.Size = new System.Drawing.Size(68, 20);
            this.metroLabelEleveMoyenne.TabIndex = 45;
            this.metroLabelEleveMoyenne.Text = "Moyenne";
            this.metroLabelEleveMoyenne.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxEleveNom
            // 
            // 
            // 
            // 
            this.metroTextBoxEleveNom.CustomButton.Image = null;
            this.metroTextBoxEleveNom.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxEleveNom.CustomButton.Name = "";
            this.metroTextBoxEleveNom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEleveNom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEleveNom.CustomButton.TabIndex = 1;
            this.metroTextBoxEleveNom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveNom.CustomButton.UseSelectable = true;
            this.metroTextBoxEleveNom.CustomButton.Visible = false;
            this.metroTextBoxEleveNom.Lines = new string[0];
            this.metroTextBoxEleveNom.Location = new System.Drawing.Point(147, 135);
            this.metroTextBoxEleveNom.MaxLength = 32767;
            this.metroTextBoxEleveNom.Name = "metroTextBoxEleveNom";
            this.metroTextBoxEleveNom.PasswordChar = '\0';
            this.metroTextBoxEleveNom.PromptText = "Insert Eleve Nom....";
            this.metroTextBoxEleveNom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEleveNom.SelectedText = "";
            this.metroTextBoxEleveNom.SelectionLength = 0;
            this.metroTextBoxEleveNom.SelectionStart = 0;
            this.metroTextBoxEleveNom.ShortcutsEnabled = true;
            this.metroTextBoxEleveNom.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxEleveNom.TabIndex = 46;
            this.metroTextBoxEleveNom.UseSelectable = true;
            this.metroTextBoxEleveNom.WaterMark = "Insert Eleve Nom....";
            this.metroTextBoxEleveNom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEleveNom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxElevePrenom
            // 
            // 
            // 
            // 
            this.metroTextBoxElevePrenom.CustomButton.Image = null;
            this.metroTextBoxElevePrenom.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxElevePrenom.CustomButton.Name = "";
            this.metroTextBoxElevePrenom.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxElevePrenom.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxElevePrenom.CustomButton.TabIndex = 1;
            this.metroTextBoxElevePrenom.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxElevePrenom.CustomButton.UseSelectable = true;
            this.metroTextBoxElevePrenom.CustomButton.Visible = false;
            this.metroTextBoxElevePrenom.Lines = new string[0];
            this.metroTextBoxElevePrenom.Location = new System.Drawing.Point(147, 181);
            this.metroTextBoxElevePrenom.MaxLength = 32767;
            this.metroTextBoxElevePrenom.Name = "metroTextBoxElevePrenom";
            this.metroTextBoxElevePrenom.PasswordChar = '\0';
            this.metroTextBoxElevePrenom.PromptText = "Insert Eleve Prenom....";
            this.metroTextBoxElevePrenom.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxElevePrenom.SelectedText = "";
            this.metroTextBoxElevePrenom.SelectionLength = 0;
            this.metroTextBoxElevePrenom.SelectionStart = 0;
            this.metroTextBoxElevePrenom.ShortcutsEnabled = true;
            this.metroTextBoxElevePrenom.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxElevePrenom.TabIndex = 47;
            this.metroTextBoxElevePrenom.UseSelectable = true;
            this.metroTextBoxElevePrenom.UseStyleColors = true;
            this.metroTextBoxElevePrenom.WaterMark = "Insert Eleve Prenom....";
            this.metroTextBoxElevePrenom.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxElevePrenom.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroDateTimeEleveDateNaiss
            // 
            this.metroDateTimeEleveDateNaiss.Location = new System.Drawing.Point(148, 221);
            this.metroDateTimeEleveDateNaiss.MinimumSize = new System.Drawing.Size(0, 30);
            this.metroDateTimeEleveDateNaiss.Name = "metroDateTimeEleveDateNaiss";
            this.metroDateTimeEleveDateNaiss.Size = new System.Drawing.Size(244, 30);
            this.metroDateTimeEleveDateNaiss.TabIndex = 48;
            // 
            // metroComboBoxEleveFormation
            // 
            this.metroComboBoxEleveFormation.AutoCompleteCustomSource.AddRange(new string[] {
            "Aucun",
            "Initiale",
            "Alternance"});
            this.metroComboBoxEleveFormation.FormattingEnabled = true;
            this.metroComboBoxEleveFormation.ItemHeight = 24;
            this.metroComboBoxEleveFormation.Items.AddRange(new object[] {
            "Aucun",
            "Initiale",
            "Alternance"});
            this.metroComboBoxEleveFormation.Location = new System.Drawing.Point(147, 268);
            this.metroComboBoxEleveFormation.Name = "metroComboBoxEleveFormation";
            this.metroComboBoxEleveFormation.PromptText = "Aucun";
            this.metroComboBoxEleveFormation.Size = new System.Drawing.Size(244, 30);
            this.metroComboBoxEleveFormation.TabIndex = 49;
            this.metroComboBoxEleveFormation.UseSelectable = true;
            // 
            // metroTextBoxEleveMoyenne
            // 
            // 
            // 
            // 
            this.metroTextBoxEleveMoyenne.CustomButton.Image = null;
            this.metroTextBoxEleveMoyenne.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxEleveMoyenne.CustomButton.Name = "";
            this.metroTextBoxEleveMoyenne.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEleveMoyenne.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEleveMoyenne.CustomButton.TabIndex = 1;
            this.metroTextBoxEleveMoyenne.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveMoyenne.CustomButton.UseSelectable = true;
            this.metroTextBoxEleveMoyenne.CustomButton.Visible = false;
            this.metroTextBoxEleveMoyenne.Lines = new string[0];
            this.metroTextBoxEleveMoyenne.Location = new System.Drawing.Point(148, 318);
            this.metroTextBoxEleveMoyenne.MaxLength = 32767;
            this.metroTextBoxEleveMoyenne.Name = "metroTextBoxEleveMoyenne";
            this.metroTextBoxEleveMoyenne.PasswordChar = '\0';
            this.metroTextBoxEleveMoyenne.PromptText = "Insert Eleve Moyenne....";
            this.metroTextBoxEleveMoyenne.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEleveMoyenne.SelectedText = "";
            this.metroTextBoxEleveMoyenne.SelectionLength = 0;
            this.metroTextBoxEleveMoyenne.SelectionStart = 0;
            this.metroTextBoxEleveMoyenne.ShortcutsEnabled = true;
            this.metroTextBoxEleveMoyenne.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxEleveMoyenne.TabIndex = 50;
            this.metroTextBoxEleveMoyenne.UseSelectable = true;
            this.metroTextBoxEleveMoyenne.WaterMark = "Insert Eleve Moyenne....";
            this.metroTextBoxEleveMoyenne.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEleveMoyenne.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonEleveAdd
            // 
            this.metroButtonEleveAdd.Location = new System.Drawing.Point(273, 406);
            this.metroButtonEleveAdd.Name = "metroButtonEleveAdd";
            this.metroButtonEleveAdd.Size = new System.Drawing.Size(118, 30);
            this.metroButtonEleveAdd.TabIndex = 51;
            this.metroButtonEleveAdd.Text = "Confirmer Add";
            this.metroButtonEleveAdd.UseSelectable = true;
            this.metroButtonEleveAdd.Click += new System.EventHandler(this.metroButtonEleveAdd_Click);
            // 
            // metroLabelEleveClasse
            // 
            this.metroLabelEleveClasse.AutoSize = true;
            this.metroLabelEleveClasse.Location = new System.Drawing.Point(41, 360);
            this.metroLabelEleveClasse.Name = "metroLabelEleveClasse";
            this.metroLabelEleveClasse.Size = new System.Drawing.Size(48, 20);
            this.metroLabelEleveClasse.TabIndex = 53;
            this.metroLabelEleveClasse.Text = "Classe";
            this.metroLabelEleveClasse.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLabelEleveClasse.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // metroComboBoxEleveClasse
            // 
            this.metroComboBoxEleveClasse.AutoCompleteCustomSource.AddRange(new string[] {
            "Aucun",
            "Initiale",
            "Alternance"});
            this.metroComboBoxEleveClasse.FormattingEnabled = true;
            this.metroComboBoxEleveClasse.ItemHeight = 24;
            this.metroComboBoxEleveClasse.Location = new System.Drawing.Point(147, 360);
            this.metroComboBoxEleveClasse.Name = "metroComboBoxEleveClasse";
            this.metroComboBoxEleveClasse.PromptText = "Aucun";
            this.metroComboBoxEleveClasse.Size = new System.Drawing.Size(244, 30);
            this.metroComboBoxEleveClasse.TabIndex = 54;
            this.metroComboBoxEleveClasse.UseSelectable = true;
            this.metroComboBoxEleveClasse.SelectedIndexChanged += new System.EventHandler(this.metroComboBox1_SelectedIndexChanged);
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataSource = typeof(EleveProfLibrary.Classe);
            // 
            // metroComboBoxMatiereClasse
            // 
            this.metroComboBoxMatiereClasse.AutoCompleteCustomSource.AddRange(new string[] {
            "Aucun",
            "Initiale",
            "Alternance"});
            this.metroComboBoxMatiereClasse.FormattingEnabled = true;
            this.metroComboBoxMatiereClasse.ItemHeight = 24;
            this.metroComboBoxMatiereClasse.Location = new System.Drawing.Point(148, 221);
            this.metroComboBoxMatiereClasse.Name = "metroComboBoxMatiereClasse";
            this.metroComboBoxMatiereClasse.PromptText = "Aucun";
            this.metroComboBoxMatiereClasse.Size = new System.Drawing.Size(244, 30);
            this.metroComboBoxMatiereClasse.TabIndex = 55;
            this.metroComboBoxMatiereClasse.UseSelectable = true;
            // 
            // metroComboBoxMatiereProf
            // 
            this.metroComboBoxMatiereProf.AutoCompleteCustomSource.AddRange(new string[] {
            "Aucun",
            "Initiale",
            "Alternance"});
            this.metroComboBoxMatiereProf.FormattingEnabled = true;
            this.metroComboBoxMatiereProf.ItemHeight = 24;
            this.metroComboBoxMatiereProf.Location = new System.Drawing.Point(147, 268);
            this.metroComboBoxMatiereProf.Name = "metroComboBoxMatiereProf";
            this.metroComboBoxMatiereProf.PromptText = "Aucun";
            this.metroComboBoxMatiereProf.Size = new System.Drawing.Size(244, 30);
            this.metroComboBoxMatiereProf.TabIndex = 56;
            this.metroComboBoxMatiereProf.UseSelectable = true;
            // 
            // metroLabelMatiereClasse
            // 
            this.metroLabelMatiereClasse.AutoSize = true;
            this.metroLabelMatiereClasse.Location = new System.Drawing.Point(42, 221);
            this.metroLabelMatiereClasse.Name = "metroLabelMatiereClasse";
            this.metroLabelMatiereClasse.Size = new System.Drawing.Size(48, 20);
            this.metroLabelMatiereClasse.TabIndex = 57;
            this.metroLabelMatiereClasse.Text = "Classe";
            this.metroLabelMatiereClasse.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelMatiereProf
            // 
            this.metroLabelMatiereProf.AutoSize = true;
            this.metroLabelMatiereProf.Location = new System.Drawing.Point(41, 268);
            this.metroLabelMatiereProf.Name = "metroLabelMatiereProf";
            this.metroLabelMatiereProf.Size = new System.Drawing.Size(75, 20);
            this.metroLabelMatiereProf.TabIndex = 58;
            this.metroLabelMatiereProf.Text = "Professeur";
            this.metroLabelMatiereProf.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // ADD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1048, 480);
            this.Controls.Add(this.metroLabelMatiereProf);
            this.Controls.Add(this.metroLabelMatiereClasse);
            this.Controls.Add(this.metroComboBoxMatiereProf);
            this.Controls.Add(this.metroComboBoxMatiereClasse);
            this.Controls.Add(this.metroComboBoxEleveClasse);
            this.Controls.Add(this.metroLabelEleveClasse);
            this.Controls.Add(this.metroButtonEleveAdd);
            this.Controls.Add(this.metroTextBoxEleveMoyenne);
            this.Controls.Add(this.metroComboBoxEleveFormation);
            this.Controls.Add(this.metroDateTimeEleveDateNaiss);
            this.Controls.Add(this.metroTextBoxElevePrenom);
            this.Controls.Add(this.metroTextBoxEleveNom);
            this.Controls.Add(this.metroLabelEleveMoyenne);
            this.Controls.Add(this.metroLabelEleveFormation);
            this.Controls.Add(this.metroLabelEleveDatNaiss);
            this.Controls.Add(this.metroLabelElevePrenom);
            this.Controls.Add(this.metroLabelEleveNom);
            this.Controls.Add(this.metroButtonAddMatiere);
            this.Controls.Add(this.metroTextBoxMatiereHeure);
            this.Controls.Add(this.metroTextBoxMatiereNom);
            this.Controls.Add(this.metroLabelMatiereHeure);
            this.Controls.Add(this.metroLabelMatiereNom);
            this.Controls.Add(this.metroButtonAddProf);
            this.Controls.Add(this.metroLabelSalaireProf);
            this.Controls.Add(this.metroTextBoxSalaireProf);
            this.Controls.Add(this.metroComboBoxProf);
            this.Controls.Add(this.metroLabelProfStatut);
            this.Controls.Add(this.metroDateTimeProf);
            this.Controls.Add(this.metroLabelDateNaiss);
            this.Controls.Add(this.metroLabelProfPrenom);
            this.Controls.Add(this.metroTextBoxProfPrenom);
            this.Controls.Add(this.metroLabelProfNom);
            this.Controls.Add(this.metroTextBoxProfNom);
            this.Controls.Add(this.metroButtonConfirmAddClasse);
            this.Controls.Add(this.metroLabelClasseSigle);
            this.Controls.Add(this.metroTextBoxClasseSigle);
            this.Controls.Add(this.metroTileMatiere);
            this.Controls.Add(this.metroTileEleve);
            this.Controls.Add(this.metroTileProf);
            this.Controls.Add(this.metroTileClasse);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Name = "ADD";
            this.Text = "Add";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.ADD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTile metroTileMatiere;
        private MetroFramework.Controls.MetroTile metroTileEleve;
        private MetroFramework.Controls.MetroTile metroTileProf;
        private MetroFramework.Controls.MetroTile metroTileClasse;
        private MetroFramework.Controls.MetroLabel metroLabelClasseSigle;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClasseSigle;
        private MetroFramework.Controls.MetroButton metroButtonConfirmAddClasse;
        private MetroFramework.Controls.MetroLabel metroLabelProfNom;
        private MetroFramework.Controls.MetroTextBox metroTextBoxProfNom;
        private MetroFramework.Controls.MetroLabel metroLabelProfPrenom;
        private MetroFramework.Controls.MetroTextBox metroTextBoxProfPrenom;
        private MetroFramework.Controls.MetroLabel metroLabelDateNaiss;
        private MetroFramework.Controls.MetroDateTime metroDateTimeProf;
        private MetroFramework.Controls.MetroLabel metroLabelProfStatut;
        private MetroFramework.Controls.MetroComboBox metroComboBoxProf;
        private MetroFramework.Controls.MetroLabel metroLabelSalaireProf;
        private MetroFramework.Controls.MetroTextBox metroTextBoxSalaireProf;
        private MetroFramework.Controls.MetroButton metroButtonAddProf;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereNom;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereHeure;
        private MetroFramework.Controls.MetroTextBox metroTextBoxMatiereNom;
        private MetroFramework.Controls.MetroTextBox metroTextBoxMatiereHeure;
        private MetroFramework.Controls.MetroButton metroButtonAddMatiere;
        private MetroFramework.Controls.MetroLabel metroLabelEleveNom;
        private MetroFramework.Controls.MetroLabel metroLabelElevePrenom;
        private MetroFramework.Controls.MetroLabel metroLabelEleveDatNaiss;
        private MetroFramework.Controls.MetroLabel metroLabelEleveFormation;
        private MetroFramework.Controls.MetroLabel metroLabelEleveMoyenne;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEleveNom;
        private MetroFramework.Controls.MetroTextBox metroTextBoxElevePrenom;
        private MetroFramework.Controls.MetroDateTime metroDateTimeEleveDateNaiss;
        private MetroFramework.Controls.MetroComboBox metroComboBoxEleveFormation;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEleveMoyenne;
        private MetroFramework.Controls.MetroButton metroButtonEleveAdd;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private MetroFramework.Controls.MetroLabel metroLabelEleveClasse;
        private MetroFramework.Controls.MetroComboBox metroComboBoxEleveClasse;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMatiereClasse;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMatiereProf;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereClasse;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereProf;
    }
}