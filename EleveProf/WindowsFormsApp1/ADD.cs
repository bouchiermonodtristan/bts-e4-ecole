﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EleveProfLibrary;

namespace WindowsFormsApp1
{
    public partial class ADD : MetroFramework.Forms.MetroForm
    {
        public ADD()
        {
            InitializeComponent();
        }

        private void ADD_Load(object sender, EventArgs e)
        {
            hideAllForms();
        }
        #region hide
        private void hideAllForms()
        {
            hideClasseForms();
            hideProfForms();
            hideMatiereForms();
            hideEleveForms();
        }
        private void hideClasseForms()
        {
            metroButtonConfirmAddClasse.Hide();
            metroTextBoxClasseSigle.Hide();
            metroLabelClasseSigle.Hide();
        }
        private void hideMatiereForms()
        {
            metroButtonAddMatiere.Hide();
            metroTextBoxMatiereHeure.Hide();
            metroTextBoxMatiereNom.Hide();
            metroLabelMatiereHeure.Hide();
            metroLabelMatiereNom.Hide();
            metroLabelMatiereClasse.Hide();
            metroLabelMatiereProf.Hide();
            metroComboBoxMatiereProf.Hide();
            metroComboBoxMatiereClasse.Hide();
        }
        private void hideProfForms()
        {
            metroLabelProfStatut.Hide();
            metroLabelDateNaiss.Hide();
            metroLabelProfPrenom.Hide();
            metroLabelProfNom.Hide();
            metroLabelSalaireProf.Hide();
            metroTextBoxProfNom.Hide();
            metroTextBoxProfPrenom.Hide();
            metroDateTimeProf.Hide();
            metroComboBoxProf.Hide();
            metroTextBoxSalaireProf.Hide();
            metroButtonAddProf.Hide();
        }
        private void hideEleveForms()
        {
            metroLabelEleveDatNaiss.Hide();
            metroDateTimeEleveDateNaiss.Hide();
            metroComboBoxEleveFormation.Hide();
            metroLabelEleveFormation.Hide();
            metroLabelEleveMoyenne.Hide();
            metroLabelEleveNom.Hide();
            metroLabelElevePrenom.Hide();
            metroTextBoxEleveNom.Hide();
            metroTextBoxElevePrenom.Hide();
            metroTextBoxEleveMoyenne.Hide();
            metroButtonEleveAdd.Hide();
            metroLabelEleveClasse.Hide();
            metroComboBoxEleveClasse.Hide();
        }
        #endregion
        #region Show
        private void showClasseForms()
        {
            metroButtonConfirmAddClasse.Show();
            metroTextBoxClasseSigle.Show();
            metroLabelClasseSigle.Show();
        }
        private void showMatiereForms()
        {
            metroButtonAddMatiere.Show();
            metroTextBoxMatiereHeure.Show();
            metroTextBoxMatiereNom.Show();
            metroLabelMatiereHeure.Show();
            metroLabelMatiereNom.Show();
            metroLabelMatiereClasse.Show();
            metroLabelMatiereProf.Show();
            metroComboBoxMatiereProf.Show();
            metroComboBoxMatiereClasse.Show();
            metroComboBoxMatiereClasse.Items.Clear();
            metroComboBoxMatiereProf.Items.Clear();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                metroComboBoxMatiereClasse.Items.Add(classe.Sigle);
            }


            GestionPersonneADO gestionPersonne = new GestionPersonneADO();
            List<Prof> listProfs = gestionPersonne.ChargerProfs();
            foreach (Prof prof in listProfs)
            {
                metroComboBoxMatiereProf.Items.Add(prof.Nom + " " + prof.Prenom + " $" + prof.Id);
            }

        }
        private void showEleveForms()
        {
            metroLabelEleveDatNaiss.Show();
            metroDateTimeEleveDateNaiss.Show();
            metroComboBoxEleveFormation.Show();
            metroLabelEleveFormation.Show();
            metroLabelEleveMoyenne.Show();
            metroLabelEleveNom.Show();
            metroLabelElevePrenom.Show();
            metroTextBoxEleveNom.Show();
            metroTextBoxElevePrenom.Show();
            metroTextBoxEleveMoyenne.Show();
            metroButtonEleveAdd.Show();
            metroLabelEleveClasse.Show();

            metroComboBoxEleveClasse.Show();
            metroComboBoxEleveClasse.Items.Clear();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                metroComboBoxEleveClasse.Items.Add(classe.Sigle);
            }
        }
        private void showProfForms()
        {
            metroLabelProfStatut.Show();
            metroLabelDateNaiss.Show();
            metroLabelProfPrenom.Show();
            metroLabelProfNom.Show();
            metroLabelSalaireProf.Show();
            metroTextBoxProfNom.Show();
            metroTextBoxProfPrenom.Show();
            metroDateTimeProf.Show();
            metroComboBoxProf.Show();
            metroTextBoxSalaireProf.Show();
            metroButtonAddProf.Show();
        }
        #endregion
        #region Menu Switch
        //Switch to Views
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form View = new VIEW();
            View.ShowDialog();
            this.Close();
        }
        //Switch to ADD
        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Add = new ADD();
            Add.ShowDialog();
            this.Close();
        }
        //Switch to UPDATE
        private void metroButton3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Update = new UPDATE();
            Update.ShowDialog();
            this.Close();
        }
        //Switch to DELETE
        private void metroButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Delete = new DELETE();
            Delete.ShowDialog();
            this.Close();
        }
        #endregion
        #region Add classe
        private void metroButtonConfirmAddClasse_Click(object sender, EventArgs e)
        {
            if (metroTextBoxClasseSigle.Text != "" && metroTextBoxClasseSigle.Text.Length < 6)
            {
                try
                {
                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    //On recupère le dernier ID rentré dans la base de donnée
                    List<Classe> listClasses = gestionClasse.ChargerClasses();
                    int Lenght = listClasses.Count();
                    //On creer la classe en auto incrémentant le dernier ID trouvé
                    Classe NewClasse = new Classe((listClasses[(Lenght-1)].Id + 1), metroTextBoxClasseSigle.Text);
                    gestionClasse.AjouterClasse(NewClasse);
                    metroTextBoxClasseSigle.Text = "";
                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY ADDED " + NewClasse.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On ADD SIGLE with value : " + metroTextBoxClasseSigle.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void metroTextBoxClasseSigle_Click(object sender, EventArgs e){}
        private void metroLabelClasseSigle_Click(object sender, EventArgs e){}
        #endregion
        #region Generate Section
        private void metroTileClasse_Click(object sender, EventArgs e)
        {
            hideAllForms();
            showClasseForms();
        }
        private void metroTileMatiere_Click(object sender, EventArgs e)
        {
            hideAllForms();
            showMatiereForms();
        }

        private void metroTileProf_Click(object sender, EventArgs e)
        {
            hideAllForms();
            showProfForms();
        }

        private void metroTileEleve_Click(object sender, EventArgs e)
        {
            hideAllForms();
            showEleveForms();
        }
        #endregion

        #region Add Prof
        private void metroButtonAddProf_Click(object sender, EventArgs e)
        {
            float Salaire = 0;
            if (metroTextBoxProfNom.Text != "" && metroTextBoxProfNom.Text.Length > 2
                && metroTextBoxProfPrenom.Text != "" && metroTextBoxProfPrenom.Text.Length > 2
                && metroDateTimeProf.Value != null
                && float.TryParse(metroTextBoxSalaireProf.Text, out Salaire) == true
                )
            {
                try
                {
                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    //On recupère le dernier ID rentré dans la base de donnée
                    List<int> nbrPersonne = gestionPersonne.ChargerPersonne();
                    int Lenght = nbrPersonne.Count();

                    Prof.TypeStatut Status;
                    if(metroComboBoxProf.Text == "Permanent")
                    {
                        Status = Prof.TypeStatut.Permanent;
                    }
                    else
                    {
                        Status = Prof.TypeStatut.Intervenant;
                    }

                    //On creer la classe en auto incrémentant le dernier ID trouvé
                    Prof NewProf = new Prof((nbrPersonne[(Lenght - 1)] + 1), 
                        metroTextBoxProfNom.Text, 
                        metroTextBoxProfPrenom.Text,
                        metroDateTimeProf.Value,
                        Status,
                        Salaire
                        );


                    gestionPersonne.AjouterProf(NewProf);

                    metroTextBoxProfPrenom.Text = "";
                    metroTextBoxProfNom.Text = "";
                    metroTextBoxSalaireProf.Text = "";

                    MetroFramework.MetroMessageBox.Show(this, "OK", "PROF SUCCESSFULLY ADDED, ID = " + NewProf.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On ADD PROF");
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroTextBoxSalaireProf_Click(object sender, EventArgs e)
        {

        }

        private void metroComboBoxProf_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void metroDateTimeProf_ValueChanged(object sender, EventArgs e)
        {

        }

        private void metroTextBoxProfPrenom_Click(object sender, EventArgs e)
        {

        }

        private void metroTextBoxProfNom_Click(object sender, EventArgs e)
        {

        }

        private void metroLabelProfStatut_Click(object sender, EventArgs e){}
        private void metroLabelDateNaiss_Click(object sender, EventArgs e){}
        private void metroLabelProfPrenom_Click(object sender, EventArgs e){}
        private void metroLabelProfNom_Click(object sender, EventArgs e){}
        private void metroLabelSalaireProf_Click(object sender, EventArgs e) { }
        #endregion
        #region Add Matiere
        private void metroButtonAddMatiere_Click(object sender, EventArgs e)
        {
            int Heures = 0;
            if (metroTextBoxMatiereHeure.Text != "" && int.TryParse(metroTextBoxMatiereHeure.Text, out Heures) == true
                && metroTextBoxMatiereNom.Text.Length > 1)
            {
                try
                {
                    GestionMatiereADO gestionMatiere = new GestionMatiereADO();
                    //On recupère le dernier ID rentré dans la base de donnée
                    List<Matiere> listMatieres = gestionMatiere.ChargerMatieres();
                    int Lenght = listMatieres.Count();

                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    List<Classe> listClasses = gestionClasse.ChargerClasses();
                    Classe SelectedClasse = null;
                    foreach (Classe classe in listClasses)
                    {
                        if (metroComboBoxMatiereClasse.Text == classe.Sigle)
                        {
                            SelectedClasse = classe;
                        }
                    }

                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    List<Prof> listProfs = gestionPersonne.ChargerProfs();

                    Prof SelectedProf = null;
                    string SelectedID = metroComboBoxMatiereProf.Text.Split('$')[1];
                    foreach (Prof prof in listProfs)
                    {
                        if (SelectedID == prof.Id.ToString())
                        {
                            SelectedProf = prof;
                        }
                    }

                    //On creer la classe en auto incrémentant le dernier ID trouvé
                    Matiere NewMatiere = new Matiere((listMatieres[(Lenght - 1)].Id + 1), 
                        metroTextBoxMatiereNom.Text,
                        Heures,
                        SelectedProf,
                        SelectedClasse
                        );
                    
                    gestionMatiere.AjouterMatiere(NewMatiere);



                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY ADDED " + NewMatiere.Id + " Selected ID Prof = "+ SelectedProf.Id + " Selected Classe ID = " + SelectedClasse.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On ADD SIGLE with value : " + metroTextBoxClasseSigle.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
        #region Add Eleve
        private void metroButtonEleveAdd_Click(object sender, EventArgs e)
        {
            float Moyenne = 0;
            if (metroTextBoxEleveNom.Text != "" && metroTextBoxEleveNom.Text.Length > 2
                && metroTextBoxElevePrenom.Text != "" && metroTextBoxElevePrenom.Text.Length > 2
                && metroDateTimeEleveDateNaiss.Value != null
                && float.TryParse(metroTextBoxEleveMoyenne.Text, out Moyenne) == true
                )
            {
                try
                {
                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    //On recupère le dernier ID rentré dans la base de donnée
                    List<int> nbrPersonne = gestionPersonne.ChargerPersonne();
                    int Lenght = nbrPersonne.Count();

                    Eleve.TypeFormation Formation;
                    if (metroComboBoxEleveFormation.Text == "Alternance")
                    {
                        Formation = Eleve.TypeFormation.Alternance;
                    }
                    else
                    {
                        Formation = Eleve.TypeFormation.Initiale;
                    }

                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    List<Classe> listClasses = gestionClasse.ChargerClasses();
                    Classe SelectedClasse = null;
                    foreach (Classe classe in listClasses)
                    {
                        if(metroComboBoxEleveClasse.Text == classe.Sigle)
                        {
                            SelectedClasse = classe;
                        }
                    }

                    //On creer la classe en auto incrémentant le dernier ID trouvé
                    Eleve NewEleve = new Eleve((nbrPersonne[(Lenght - 1)] + 1),
                        metroTextBoxEleveNom.Text,
                        metroTextBoxElevePrenom.Text,
                        metroDateTimeEleveDateNaiss.Value,
                        Formation,
                        Moyenne,
                        SelectedClasse
                        );


                    gestionPersonne.AjouterEleve(NewEleve);

                    metroTextBoxElevePrenom.Text = "";
                    metroTextBoxEleveNom.Text = "";
                    metroTextBoxEleveMoyenne.Text = "";

                    MetroFramework.MetroMessageBox.Show(this, "OK", "ELEVE SUCCESSFULLY ADDED, ID = " + NewEleve.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On ADD ELEVE ");
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
