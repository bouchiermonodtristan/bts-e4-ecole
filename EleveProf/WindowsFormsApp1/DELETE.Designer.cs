﻿namespace WindowsFormsApp1
{
    partial class DELETE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxClasse = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelClasse = new MetroFramework.Controls.MetroLabel();
            this.metroButtonDelete = new MetroFramework.Controls.MetroButton();
            this.metroButtonDeleteProf = new MetroFramework.Controls.MetroButton();
            this.metroLabelDeleteProf = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDeleteProf = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonDeleteEleve = new MetroFramework.Controls.MetroButton();
            this.metroLabelDeleteEleve = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDeleteEleve = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonDeleteMatiere = new MetroFramework.Controls.MetroButton();
            this.metroLabelDeleteMatiere = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxDeleteMatiere = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(398, 62);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(119, 37);
            this.metroButton4.TabIndex = 13;
            this.metroButton4.Text = "Delete";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(273, 62);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(119, 37);
            this.metroButton3.TabIndex = 12;
            this.metroButton3.Text = "Update";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(148, 63);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(119, 37);
            this.metroButton2.TabIndex = 11;
            this.metroButton2.Text = "Add";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(23, 63);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(119, 37);
            this.metroButton1.TabIndex = 10;
            this.metroButton1.Text = "View";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroTextBoxClasse
            // 
            // 
            // 
            // 
            this.metroTextBoxClasse.CustomButton.Image = null;
            this.metroTextBoxClasse.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxClasse.CustomButton.Name = "";
            this.metroTextBoxClasse.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxClasse.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClasse.CustomButton.TabIndex = 1;
            this.metroTextBoxClasse.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClasse.CustomButton.UseSelectable = true;
            this.metroTextBoxClasse.CustomButton.Visible = false;
            this.metroTextBoxClasse.Lines = new string[0];
            this.metroTextBoxClasse.Location = new System.Drawing.Point(148, 117);
            this.metroTextBoxClasse.MaxLength = 32767;
            this.metroTextBoxClasse.Name = "metroTextBoxClasse";
            this.metroTextBoxClasse.PasswordChar = '\0';
            this.metroTextBoxClasse.PromptText = "Insert Classe ID....";
            this.metroTextBoxClasse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClasse.SelectedText = "";
            this.metroTextBoxClasse.SelectionLength = 0;
            this.metroTextBoxClasse.SelectionStart = 0;
            this.metroTextBoxClasse.ShortcutsEnabled = true;
            this.metroTextBoxClasse.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxClasse.TabIndex = 14;
            this.metroTextBoxClasse.UseSelectable = true;
            this.metroTextBoxClasse.WaterMark = "Insert Classe ID....";
            this.metroTextBoxClasse.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClasse.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxClasse.Click += new System.EventHandler(this.metroTextBoxClasse_Click);
            // 
            // metroLabelClasse
            // 
            this.metroLabelClasse.AutoSize = true;
            this.metroLabelClasse.Location = new System.Drawing.Point(41, 114);
            this.metroLabelClasse.Name = "metroLabelClasse";
            this.metroLabelClasse.Size = new System.Drawing.Size(65, 20);
            this.metroLabelClasse.TabIndex = 19;
            this.metroLabelClasse.Text = "Classe ID";
            this.metroLabelClasse.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButtonDelete
            // 
            this.metroButtonDelete.Location = new System.Drawing.Point(433, 117);
            this.metroButtonDelete.Name = "metroButtonDelete";
            this.metroButtonDelete.Size = new System.Drawing.Size(119, 23);
            this.metroButtonDelete.TabIndex = 20;
            this.metroButtonDelete.Text = "Confirm Delete";
            this.metroButtonDelete.UseSelectable = true;
            this.metroButtonDelete.Click += new System.EventHandler(this.metroButtonDelete_Click);
            // 
            // metroButtonDeleteProf
            // 
            this.metroButtonDeleteProf.Location = new System.Drawing.Point(433, 155);
            this.metroButtonDeleteProf.Name = "metroButtonDeleteProf";
            this.metroButtonDeleteProf.Size = new System.Drawing.Size(119, 23);
            this.metroButtonDeleteProf.TabIndex = 23;
            this.metroButtonDeleteProf.Text = "Confirm Delete";
            this.metroButtonDeleteProf.UseSelectable = true;
            this.metroButtonDeleteProf.Click += new System.EventHandler(this.metroButtonDeleteProf_Click);
            // 
            // metroLabelDeleteProf
            // 
            this.metroLabelDeleteProf.AutoSize = true;
            this.metroLabelDeleteProf.Location = new System.Drawing.Point(55, 152);
            this.metroLabelDeleteProf.Name = "metroLabelDeleteProf";
            this.metroLabelDeleteProf.Size = new System.Drawing.Size(51, 20);
            this.metroLabelDeleteProf.TabIndex = 22;
            this.metroLabelDeleteProf.Text = "Prof ID";
            this.metroLabelDeleteProf.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxDeleteProf
            // 
            // 
            // 
            // 
            this.metroTextBoxDeleteProf.CustomButton.Image = null;
            this.metroTextBoxDeleteProf.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxDeleteProf.CustomButton.Name = "";
            this.metroTextBoxDeleteProf.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxDeleteProf.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxDeleteProf.CustomButton.TabIndex = 1;
            this.metroTextBoxDeleteProf.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxDeleteProf.CustomButton.UseSelectable = true;
            this.metroTextBoxDeleteProf.CustomButton.Visible = false;
            this.metroTextBoxDeleteProf.Lines = new string[0];
            this.metroTextBoxDeleteProf.Location = new System.Drawing.Point(148, 155);
            this.metroTextBoxDeleteProf.MaxLength = 32767;
            this.metroTextBoxDeleteProf.Name = "metroTextBoxDeleteProf";
            this.metroTextBoxDeleteProf.PasswordChar = '\0';
            this.metroTextBoxDeleteProf.PromptText = "Insert Prof ID....";
            this.metroTextBoxDeleteProf.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxDeleteProf.SelectedText = "";
            this.metroTextBoxDeleteProf.SelectionLength = 0;
            this.metroTextBoxDeleteProf.SelectionStart = 0;
            this.metroTextBoxDeleteProf.ShortcutsEnabled = true;
            this.metroTextBoxDeleteProf.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxDeleteProf.TabIndex = 21;
            this.metroTextBoxDeleteProf.UseSelectable = true;
            this.metroTextBoxDeleteProf.WaterMark = "Insert Prof ID....";
            this.metroTextBoxDeleteProf.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxDeleteProf.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxDeleteProf.Click += new System.EventHandler(this.metroTextBoxDeleteProf_Click);
            // 
            // metroButtonDeleteEleve
            // 
            this.metroButtonDeleteEleve.Location = new System.Drawing.Point(433, 196);
            this.metroButtonDeleteEleve.Name = "metroButtonDeleteEleve";
            this.metroButtonDeleteEleve.Size = new System.Drawing.Size(119, 23);
            this.metroButtonDeleteEleve.TabIndex = 26;
            this.metroButtonDeleteEleve.Text = "Confirm Delete";
            this.metroButtonDeleteEleve.UseSelectable = true;
            this.metroButtonDeleteEleve.Click += new System.EventHandler(this.metroButtonDeleteEleve_Click);
            // 
            // metroLabelDeleteEleve
            // 
            this.metroLabelDeleteEleve.AutoSize = true;
            this.metroLabelDeleteEleve.Location = new System.Drawing.Point(46, 193);
            this.metroLabelDeleteEleve.Name = "metroLabelDeleteEleve";
            this.metroLabelDeleteEleve.Size = new System.Drawing.Size(60, 20);
            this.metroLabelDeleteEleve.TabIndex = 25;
            this.metroLabelDeleteEleve.Text = "Eleve ID";
            this.metroLabelDeleteEleve.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxDeleteEleve
            // 
            // 
            // 
            // 
            this.metroTextBoxDeleteEleve.CustomButton.Image = null;
            this.metroTextBoxDeleteEleve.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxDeleteEleve.CustomButton.Name = "";
            this.metroTextBoxDeleteEleve.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxDeleteEleve.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxDeleteEleve.CustomButton.TabIndex = 1;
            this.metroTextBoxDeleteEleve.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxDeleteEleve.CustomButton.UseSelectable = true;
            this.metroTextBoxDeleteEleve.CustomButton.Visible = false;
            this.metroTextBoxDeleteEleve.Lines = new string[0];
            this.metroTextBoxDeleteEleve.Location = new System.Drawing.Point(148, 196);
            this.metroTextBoxDeleteEleve.MaxLength = 32767;
            this.metroTextBoxDeleteEleve.Name = "metroTextBoxDeleteEleve";
            this.metroTextBoxDeleteEleve.PasswordChar = '\0';
            this.metroTextBoxDeleteEleve.PromptText = "Insert Eleve ID....";
            this.metroTextBoxDeleteEleve.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxDeleteEleve.SelectedText = "";
            this.metroTextBoxDeleteEleve.SelectionLength = 0;
            this.metroTextBoxDeleteEleve.SelectionStart = 0;
            this.metroTextBoxDeleteEleve.ShortcutsEnabled = true;
            this.metroTextBoxDeleteEleve.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxDeleteEleve.TabIndex = 24;
            this.metroTextBoxDeleteEleve.UseSelectable = true;
            this.metroTextBoxDeleteEleve.WaterMark = "Insert Eleve ID....";
            this.metroTextBoxDeleteEleve.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxDeleteEleve.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxDeleteEleve.Click += new System.EventHandler(this.metroTextBoxDeleteEleve_Click);
            // 
            // metroButtonDeleteMatiere
            // 
            this.metroButtonDeleteMatiere.Location = new System.Drawing.Point(433, 237);
            this.metroButtonDeleteMatiere.Name = "metroButtonDeleteMatiere";
            this.metroButtonDeleteMatiere.Size = new System.Drawing.Size(119, 23);
            this.metroButtonDeleteMatiere.TabIndex = 29;
            this.metroButtonDeleteMatiere.Text = "Confirm Delete";
            this.metroButtonDeleteMatiere.UseSelectable = true;
            this.metroButtonDeleteMatiere.Click += new System.EventHandler(this.metroButtonDeleteMatiere_Click);
            // 
            // metroLabelDeleteMatiere
            // 
            this.metroLabelDeleteMatiere.AutoSize = true;
            this.metroLabelDeleteMatiere.Location = new System.Drawing.Point(33, 237);
            this.metroLabelDeleteMatiere.Name = "metroLabelDeleteMatiere";
            this.metroLabelDeleteMatiere.Size = new System.Drawing.Size(73, 20);
            this.metroLabelDeleteMatiere.TabIndex = 28;
            this.metroLabelDeleteMatiere.Text = "Matiere ID";
            this.metroLabelDeleteMatiere.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxDeleteMatiere
            // 
            // 
            // 
            // 
            this.metroTextBoxDeleteMatiere.CustomButton.Image = null;
            this.metroTextBoxDeleteMatiere.CustomButton.Location = new System.Drawing.Point(222, 1);
            this.metroTextBoxDeleteMatiere.CustomButton.Name = "";
            this.metroTextBoxDeleteMatiere.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxDeleteMatiere.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxDeleteMatiere.CustomButton.TabIndex = 1;
            this.metroTextBoxDeleteMatiere.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxDeleteMatiere.CustomButton.UseSelectable = true;
            this.metroTextBoxDeleteMatiere.CustomButton.Visible = false;
            this.metroTextBoxDeleteMatiere.Lines = new string[0];
            this.metroTextBoxDeleteMatiere.Location = new System.Drawing.Point(148, 237);
            this.metroTextBoxDeleteMatiere.MaxLength = 32767;
            this.metroTextBoxDeleteMatiere.Name = "metroTextBoxDeleteMatiere";
            this.metroTextBoxDeleteMatiere.PasswordChar = '\0';
            this.metroTextBoxDeleteMatiere.PromptText = "Insert Matiere ID....";
            this.metroTextBoxDeleteMatiere.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxDeleteMatiere.SelectedText = "";
            this.metroTextBoxDeleteMatiere.SelectionLength = 0;
            this.metroTextBoxDeleteMatiere.SelectionStart = 0;
            this.metroTextBoxDeleteMatiere.ShortcutsEnabled = true;
            this.metroTextBoxDeleteMatiere.Size = new System.Drawing.Size(244, 23);
            this.metroTextBoxDeleteMatiere.TabIndex = 27;
            this.metroTextBoxDeleteMatiere.UseSelectable = true;
            this.metroTextBoxDeleteMatiere.WaterMark = "Insert Matiere ID....";
            this.metroTextBoxDeleteMatiere.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxDeleteMatiere.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metroTextBoxDeleteMatiere.Click += new System.EventHandler(this.metroTextBoxDeleteMatiere_Click);
            // 
            // DELETE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1048, 480);
            this.Controls.Add(this.metroButtonDeleteMatiere);
            this.Controls.Add(this.metroLabelDeleteMatiere);
            this.Controls.Add(this.metroTextBoxDeleteMatiere);
            this.Controls.Add(this.metroButtonDeleteEleve);
            this.Controls.Add(this.metroLabelDeleteEleve);
            this.Controls.Add(this.metroTextBoxDeleteEleve);
            this.Controls.Add(this.metroButtonDeleteProf);
            this.Controls.Add(this.metroLabelDeleteProf);
            this.Controls.Add(this.metroTextBoxDeleteProf);
            this.Controls.Add(this.metroButtonDelete);
            this.Controls.Add(this.metroLabelClasse);
            this.Controls.Add(this.metroTextBoxClasse);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Name = "DELETE";
            this.Text = "Delete";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClasse;
        private MetroFramework.Controls.MetroLabel metroLabelClasse;
        private MetroFramework.Controls.MetroButton metroButtonDelete;
        private MetroFramework.Controls.MetroButton metroButtonDeleteProf;
        private MetroFramework.Controls.MetroLabel metroLabelDeleteProf;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDeleteProf;
        private MetroFramework.Controls.MetroButton metroButtonDeleteEleve;
        private MetroFramework.Controls.MetroLabel metroLabelDeleteEleve;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDeleteEleve;
        private MetroFramework.Controls.MetroButton metroButtonDeleteMatiere;
        private MetroFramework.Controls.MetroLabel metroLabelDeleteMatiere;
        private MetroFramework.Controls.MetroTextBox metroTextBoxDeleteMatiere;
    }
}