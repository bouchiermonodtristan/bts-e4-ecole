﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EleveProfLibrary;

namespace WindowsFormsApp1
{
    public partial class DELETE : MetroFramework.Forms.MetroForm
    {
        public DELETE()
        {
            InitializeComponent();
        }
        #region Menu Switch
        //Switch to Views
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form View = new VIEW();
            View.ShowDialog();
            this.Close();
        }
        //Switch to ADD
        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Add = new ADD();
            Add.ShowDialog();
            this.Close();
        }
        //Switch to UPDATE
        private void metroButton3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Update = new UPDATE();
            Update.ShowDialog();
            this.Close();
        }
        //Switch to DELETE
        private void metroButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Delete = new DELETE();
            Delete.ShowDialog();
            this.Close();
        }
        #endregion

        #region Click Classe

        private void metroButtonDelete_Click(object sender, EventArgs e)
        {
            GestionClasseADO gestionClasse = new GestionClasseADO();
            if (metroTextBoxClasse.Text != "")
            {
                try
                {
                    gestionClasse.SupprimerClasse(Int32.Parse(metroTextBoxClasse.Text));
                    metroTextBoxClasse.Text = "";
                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY DELETED", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new Exception("Error : " + ex + " On DELETE CLASSE with value : " + metroTextBoxClasse.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void metroTextBoxClasse_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region Click Prof
        private void metroButtonDeleteProf_Click(object sender, EventArgs e)
        {
            GestionPersonneADO gestionProf = new GestionPersonneADO();
            if (metroTextBoxDeleteProf.Text != "")
            {
                try
                {
                    gestionProf.SupprimerProf(Int32.Parse(metroTextBoxDeleteProf.Text));
                    metroTextBoxDeleteProf.Text = "";
                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY DELETED", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new Exception("Error : " + ex + " On DELETE PROF with value : " + metroTextBoxDeleteProf.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroTextBoxDeleteProf_Click(object sender, EventArgs e)
        {
        }
        #endregion

        #region Click Eleve
        private void metroButtonDeleteEleve_Click(object sender, EventArgs e)
        {
            GestionPersonneADO gestionEleve = new GestionPersonneADO();
            if (metroTextBoxDeleteEleve.Text != "")
            {
                try
                {
                    gestionEleve.SupprimerEleve(Int32.Parse(metroTextBoxDeleteEleve.Text));
                    metroTextBoxDeleteEleve.Text = "";
                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY DELETED", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw new Exception("Error : " + ex + " On DELETE ELEVE with value : " + metroTextBoxDeleteEleve.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroTextBoxDeleteEleve_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region Click Matiere
        /** TODO
         * 
         * */
        private void metroButtonDeleteMatiere_Click(object sender, EventArgs e)
        {
            GestionMatiereADO gestionMatiere = new GestionMatiereADO();
            /*  if (metroTextBoxDeleteMatiere.Text != "")
              {
                  try
                  {
                      gestionMatiere.SupprimerMatiere(Int32.Parse(metroTextBoxDeleteMatiere.Text));
                      metroTextBoxDeleteMatiere.Text = "";
                      MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY DELETED", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
                  }
                  catch (Exception ex)
                  {
                      MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                      throw new Exception("Error : " + ex + " On DELETE MATIERE with value : "+metroTextBoxDeleteMatiere.Text);
                  }
              }
              else
              {
                  MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
              }*/
        }

        private void metroTextBoxDeleteMatiere_Click(object sender, EventArgs e)
        {

        }
        #endregion
    }
}
