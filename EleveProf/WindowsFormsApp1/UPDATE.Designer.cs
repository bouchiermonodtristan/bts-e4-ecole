﻿using System;

namespace WindowsFormsApp1
{
    partial class UPDATE
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTileClasse = new MetroFramework.Controls.MetroTile();
            this.metroListViewClasse = new MetroFramework.Controls.MetroListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroTileProf = new MetroFramework.Controls.MetroTile();
            this.metroTileEleve = new MetroFramework.Controls.MetroTile();
            this.metroTileMatiere = new MetroFramework.Controls.MetroTile();
            this.metroListViewProf = new MetroFramework.Controls.MetroListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroListViewEleve = new MetroFramework.Controls.MetroListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroListViewMatiere = new MetroFramework.Controls.MetroListView();
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroLabelClasseId = new MetroFramework.Controls.MetroLabel();
            this.metroLabelClasseIdSelected = new MetroFramework.Controls.MetroLabel();
            this.metroLabelClasseSigle = new MetroFramework.Controls.MetroLabel();
            this.metroButtonClasseUpdate = new MetroFramework.Controls.MetroButton();
            this.metroLabelEleveId = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveIdSelected = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveNom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelElevePrenom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveDateNaiss = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveFormation = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveMoyenne = new MetroFramework.Controls.MetroLabel();
            this.metroLabelEleveClasse = new MetroFramework.Controls.MetroLabel();
            this.metroButtonEleveUpdate = new MetroFramework.Controls.MetroButton();
            this.metroTextBoxEleveNomSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxElevePrenomSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxEleveMoyenneSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroDateTimeEleveDateNaissSelected = new MetroFramework.Controls.MetroDateTime();
            this.metroComboBoxEleveClasseSelected = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBoxEleveFormationSelected = new MetroFramework.Controls.MetroComboBox();
            this.metroTextBoxClasseSigleSelected = new MetroFramework.Controls.MetroTextBox();
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroLabelProfId = new MetroFramework.Controls.MetroLabel();
            this.metroLabelProfIdSelected = new MetroFramework.Controls.MetroLabel();
            this.metroLabelProfNom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelProfPrenom = new MetroFramework.Controls.MetroLabel();
            this.metroLabelProfDateNaiss = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxProfNomSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBoxProfPrenomSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroDateTimeProfDateNaissSelected = new MetroFramework.Controls.MetroDateTime();
            this.metroLabelProfStatut = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxProfStatutSelected = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelProfSalaire = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxProfSalaireSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroButtonProfUpdate = new MetroFramework.Controls.MetroButton();
            this.metroLabelMatiereId = new MetroFramework.Controls.MetroLabel();
            this.metroLabelMatiereIdSelected = new MetroFramework.Controls.MetroLabel();
            this.metroLabelMatiereNom = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxMatiereNomSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelMatiereHeure = new MetroFramework.Controls.MetroLabel();
            this.metroTextBoxMatiereHeureSelected = new MetroFramework.Controls.MetroTextBox();
            this.metroLabelMatiereClasse = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxMatiereClasseSelected = new MetroFramework.Controls.MetroComboBox();
            this.metroLabelMatiereProf = new MetroFramework.Controls.MetroLabel();
            this.metroComboBoxMatiereProfSelected = new MetroFramework.Controls.MetroComboBox();
            this.metroButtonMatiereUpdate = new MetroFramework.Controls.MetroButton();
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // metroTileClasse
            // 
            this.metroTileClasse.ActiveControl = null;
            this.metroTileClasse.Location = new System.Drawing.Point(694, 64);
            this.metroTileClasse.Name = "metroTileClasse";
            this.metroTileClasse.Size = new System.Drawing.Size(151, 44);
            this.metroTileClasse.TabIndex = 0;
            this.metroTileClasse.Text = "Generate Classe";
            this.metroTileClasse.UseSelectable = true;
            this.metroTileClasse.Click += new System.EventHandler(this.metroTileClasse_Click);
            // 
            // metroListViewClasse
            // 
            this.metroListViewClasse.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.metroListViewClasse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewClasse.FullRowSelect = true;
            this.metroListViewClasse.Location = new System.Drawing.Point(23, 107);
            this.metroListViewClasse.Name = "metroListViewClasse";
            this.metroListViewClasse.OwnerDraw = true;
            this.metroListViewClasse.Size = new System.Drawing.Size(334, 305);
            this.metroListViewClasse.TabIndex = 1;
            this.metroListViewClasse.UseCompatibleStateImageBehavior = false;
            this.metroListViewClasse.UseSelectable = true;
            this.metroListViewClasse.View = System.Windows.Forms.View.Details;
            this.metroListViewClasse.ItemActivate += new System.EventHandler(this.metroListViewClasse_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Classe iD";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Sigle";
            this.columnHeader2.Width = 100;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(24, 64);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(119, 37);
            this.metroButton1.TabIndex = 2;
            this.metroButton1.Text = "View";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(149, 64);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(119, 37);
            this.metroButton2.TabIndex = 3;
            this.metroButton2.Text = "Add";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(274, 63);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(119, 37);
            this.metroButton3.TabIndex = 4;
            this.metroButton3.Text = "Update";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(399, 63);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(119, 37);
            this.metroButton4.TabIndex = 5;
            this.metroButton4.Text = "Delete";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroTileProf
            // 
            this.metroTileProf.ActiveControl = null;
            this.metroTileProf.Location = new System.Drawing.Point(694, 114);
            this.metroTileProf.Name = "metroTileProf";
            this.metroTileProf.Size = new System.Drawing.Size(151, 45);
            this.metroTileProf.TabIndex = 0;
            this.metroTileProf.Text = "Generate Prof";
            this.metroTileProf.UseSelectable = true;
            this.metroTileProf.Click += new System.EventHandler(this.metroTileProf_Click);
            // 
            // metroTileEleve
            // 
            this.metroTileEleve.ActiveControl = null;
            this.metroTileEleve.Location = new System.Drawing.Point(851, 114);
            this.metroTileEleve.Name = "metroTileEleve";
            this.metroTileEleve.Size = new System.Drawing.Size(151, 45);
            this.metroTileEleve.TabIndex = 6;
            this.metroTileEleve.Text = "Generate Eleve";
            this.metroTileEleve.UseSelectable = true;
            this.metroTileEleve.Click += new System.EventHandler(this.metroTileEleve_Click);
            // 
            // metroTileMatiere
            // 
            this.metroTileMatiere.ActiveControl = null;
            this.metroTileMatiere.Location = new System.Drawing.Point(852, 64);
            this.metroTileMatiere.Name = "metroTileMatiere";
            this.metroTileMatiere.Size = new System.Drawing.Size(150, 44);
            this.metroTileMatiere.TabIndex = 7;
            this.metroTileMatiere.Text = "Generate Matiere";
            this.metroTileMatiere.UseSelectable = true;
            this.metroTileMatiere.Click += new System.EventHandler(this.metroTileMatiere_Click);
            // 
            // metroListViewProf
            // 
            this.metroListViewProf.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.metroListViewProf.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewProf.FullRowSelect = true;
            this.metroListViewProf.Location = new System.Drawing.Point(24, 107);
            this.metroListViewProf.Name = "metroListViewProf";
            this.metroListViewProf.OwnerDraw = true;
            this.metroListViewProf.Size = new System.Drawing.Size(333, 305);
            this.metroListViewProf.TabIndex = 8;
            this.metroListViewProf.UseCompatibleStateImageBehavior = false;
            this.metroListViewProf.UseSelectable = true;
            this.metroListViewProf.View = System.Windows.Forms.View.Details;
            this.metroListViewProf.ItemActivate += new System.EventHandler(this.metroListViewProf_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Prof ID";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Nom";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Prenom";
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "DateNaiss";
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Statut";
            this.columnHeader7.Width = 100;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Salaire";
            this.columnHeader8.Width = 100;
            // 
            // metroListViewEleve
            // 
            this.metroListViewEleve.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader18});
            this.metroListViewEleve.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewEleve.FullRowSelect = true;
            this.metroListViewEleve.Location = new System.Drawing.Point(23, 106);
            this.metroListViewEleve.Name = "metroListViewEleve";
            this.metroListViewEleve.OwnerDraw = true;
            this.metroListViewEleve.Size = new System.Drawing.Size(334, 304);
            this.metroListViewEleve.TabIndex = 9;
            this.metroListViewEleve.UseCompatibleStateImageBehavior = false;
            this.metroListViewEleve.UseSelectable = true;
            this.metroListViewEleve.View = System.Windows.Forms.View.Details;
            this.metroListViewEleve.ItemActivate += new System.EventHandler(this.metroListViewEleve_SelectedIndexChanged);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "ID";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Nom";
            this.columnHeader10.Width = 100;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Prenom";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "DateNaiss";
            this.columnHeader12.Width = 100;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Formation";
            this.columnHeader13.Width = 109;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Moyenne";
            this.columnHeader14.Width = 100;
            // 
            // metroListViewMatiere
            // 
            this.metroListViewMatiere.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader19,
            this.columnHeader20});
            this.metroListViewMatiere.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewMatiere.FullRowSelect = true;
            this.metroListViewMatiere.Location = new System.Drawing.Point(24, 106);
            this.metroListViewMatiere.Name = "metroListViewMatiere";
            this.metroListViewMatiere.OwnerDraw = true;
            this.metroListViewMatiere.Size = new System.Drawing.Size(334, 306);
            this.metroListViewMatiere.TabIndex = 10;
            this.metroListViewMatiere.UseCompatibleStateImageBehavior = false;
            this.metroListViewMatiere.UseSelectable = true;
            this.metroListViewMatiere.View = System.Windows.Forms.View.Details;
            this.metroListViewMatiere.ItemActivate += new System.EventHandler(this.metroListViewMatiere_SelectedIndexChanged);
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "ID";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Matiere";
            this.columnHeader16.Width = 120;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Heures";
            this.columnHeader17.Width = 124;
            // 
            // metroLabelClasseId
            // 
            this.metroLabelClasseId.AutoSize = true;
            this.metroLabelClasseId.Location = new System.Drawing.Point(694, 191);
            this.metroLabelClasseId.Name = "metroLabelClasseId";
            this.metroLabelClasseId.Size = new System.Drawing.Size(65, 20);
            this.metroLabelClasseId.TabIndex = 11;
            this.metroLabelClasseId.Text = "Classe ID";
            this.metroLabelClasseId.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelClasseIdSelected
            // 
            this.metroLabelClasseIdSelected.AutoSize = true;
            this.metroLabelClasseIdSelected.Location = new System.Drawing.Point(791, 191);
            this.metroLabelClasseIdSelected.Name = "metroLabelClasseIdSelected";
            this.metroLabelClasseIdSelected.Size = new System.Drawing.Size(34, 20);
            this.metroLabelClasseIdSelected.TabIndex = 12;
            this.metroLabelClasseIdSelected.Text = "N/A";
            this.metroLabelClasseIdSelected.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelClasseSigle
            // 
            this.metroLabelClasseSigle.AutoSize = true;
            this.metroLabelClasseSigle.Location = new System.Drawing.Point(694, 234);
            this.metroLabelClasseSigle.Name = "metroLabelClasseSigle";
            this.metroLabelClasseSigle.Size = new System.Drawing.Size(38, 20);
            this.metroLabelClasseSigle.TabIndex = 13;
            this.metroLabelClasseSigle.Text = "Sigle";
            this.metroLabelClasseSigle.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButtonClasseUpdate
            // 
            this.metroButtonClasseUpdate.Location = new System.Drawing.Point(791, 286);
            this.metroButtonClasseUpdate.Name = "metroButtonClasseUpdate";
            this.metroButtonClasseUpdate.Size = new System.Drawing.Size(89, 27);
            this.metroButtonClasseUpdate.TabIndex = 14;
            this.metroButtonClasseUpdate.Text = "Update";
            this.metroButtonClasseUpdate.UseSelectable = true;
            this.metroButtonClasseUpdate.Click += new System.EventHandler(this.metroButtonClasseUpdate_Click);
            // 
            // metroLabelEleveId
            // 
            this.metroLabelEleveId.AutoSize = true;
            this.metroLabelEleveId.Location = new System.Drawing.Point(399, 139);
            this.metroLabelEleveId.Name = "metroLabelEleveId";
            this.metroLabelEleveId.Size = new System.Drawing.Size(60, 20);
            this.metroLabelEleveId.TabIndex = 16;
            this.metroLabelEleveId.Text = "Eleve ID";
            this.metroLabelEleveId.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveIdSelected
            // 
            this.metroLabelEleveIdSelected.AutoSize = true;
            this.metroLabelEleveIdSelected.Location = new System.Drawing.Point(484, 139);
            this.metroLabelEleveIdSelected.Name = "metroLabelEleveIdSelected";
            this.metroLabelEleveIdSelected.Size = new System.Drawing.Size(34, 20);
            this.metroLabelEleveIdSelected.TabIndex = 17;
            this.metroLabelEleveIdSelected.Text = "N/A";
            this.metroLabelEleveIdSelected.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveNom
            // 
            this.metroLabelEleveNom.AutoSize = true;
            this.metroLabelEleveNom.Location = new System.Drawing.Point(399, 183);
            this.metroLabelEleveNom.Name = "metroLabelEleveNom";
            this.metroLabelEleveNom.Size = new System.Drawing.Size(40, 20);
            this.metroLabelEleveNom.TabIndex = 18;
            this.metroLabelEleveNom.Text = "Nom";
            this.metroLabelEleveNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelElevePrenom
            // 
            this.metroLabelElevePrenom.AutoSize = true;
            this.metroLabelElevePrenom.Location = new System.Drawing.Point(399, 224);
            this.metroLabelElevePrenom.Name = "metroLabelElevePrenom";
            this.metroLabelElevePrenom.Size = new System.Drawing.Size(58, 20);
            this.metroLabelElevePrenom.TabIndex = 19;
            this.metroLabelElevePrenom.Text = "Prenom";
            this.metroLabelElevePrenom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveDateNaiss
            // 
            this.metroLabelEleveDateNaiss.AutoSize = true;
            this.metroLabelEleveDateNaiss.Location = new System.Drawing.Point(399, 270);
            this.metroLabelEleveDateNaiss.Name = "metroLabelEleveDateNaiss";
            this.metroLabelEleveDateNaiss.Size = new System.Drawing.Size(75, 20);
            this.metroLabelEleveDateNaiss.TabIndex = 20;
            this.metroLabelEleveDateNaiss.Text = "Date Naiss";
            this.metroLabelEleveDateNaiss.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveFormation
            // 
            this.metroLabelEleveFormation.AutoSize = true;
            this.metroLabelEleveFormation.Location = new System.Drawing.Point(399, 317);
            this.metroLabelEleveFormation.Name = "metroLabelEleveFormation";
            this.metroLabelEleveFormation.Size = new System.Drawing.Size(71, 20);
            this.metroLabelEleveFormation.TabIndex = 21;
            this.metroLabelEleveFormation.Text = "Formation";
            this.metroLabelEleveFormation.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveMoyenne
            // 
            this.metroLabelEleveMoyenne.AutoSize = true;
            this.metroLabelEleveMoyenne.Location = new System.Drawing.Point(399, 358);
            this.metroLabelEleveMoyenne.Name = "metroLabelEleveMoyenne";
            this.metroLabelEleveMoyenne.Size = new System.Drawing.Size(68, 20);
            this.metroLabelEleveMoyenne.TabIndex = 22;
            this.metroLabelEleveMoyenne.Text = "Moyenne";
            this.metroLabelEleveMoyenne.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelEleveClasse
            // 
            this.metroLabelEleveClasse.AutoSize = true;
            this.metroLabelEleveClasse.Location = new System.Drawing.Point(399, 404);
            this.metroLabelEleveClasse.Name = "metroLabelEleveClasse";
            this.metroLabelEleveClasse.Size = new System.Drawing.Size(48, 20);
            this.metroLabelEleveClasse.TabIndex = 23;
            this.metroLabelEleveClasse.Text = "Classe";
            this.metroLabelEleveClasse.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButtonEleveUpdate
            // 
            this.metroButtonEleveUpdate.Location = new System.Drawing.Point(553, 439);
            this.metroButtonEleveUpdate.Name = "metroButtonEleveUpdate";
            this.metroButtonEleveUpdate.Size = new System.Drawing.Size(89, 27);
            this.metroButtonEleveUpdate.TabIndex = 24;
            this.metroButtonEleveUpdate.Text = "Update";
            this.metroButtonEleveUpdate.UseSelectable = true;
            this.metroButtonEleveUpdate.Click += new System.EventHandler(this.metroButtonEleveUpdate_Click);
            // 
            // metroTextBoxEleveNomSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxEleveNomSelected.CustomButton.Image = null;
            this.metroTextBoxEleveNomSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxEleveNomSelected.CustomButton.Name = "";
            this.metroTextBoxEleveNomSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEleveNomSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEleveNomSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxEleveNomSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveNomSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxEleveNomSelected.CustomButton.Visible = false;
            this.metroTextBoxEleveNomSelected.Lines = new string[0];
            this.metroTextBoxEleveNomSelected.Location = new System.Drawing.Point(484, 180);
            this.metroTextBoxEleveNomSelected.MaxLength = 32767;
            this.metroTextBoxEleveNomSelected.Name = "metroTextBoxEleveNomSelected";
            this.metroTextBoxEleveNomSelected.PasswordChar = '\0';
            this.metroTextBoxEleveNomSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEleveNomSelected.SelectedText = "";
            this.metroTextBoxEleveNomSelected.SelectionLength = 0;
            this.metroTextBoxEleveNomSelected.SelectionStart = 0;
            this.metroTextBoxEleveNomSelected.ShortcutsEnabled = true;
            this.metroTextBoxEleveNomSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxEleveNomSelected.TabIndex = 25;
            this.metroTextBoxEleveNomSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveNomSelected.UseSelectable = true;
            this.metroTextBoxEleveNomSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEleveNomSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxElevePrenomSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxElevePrenomSelected.CustomButton.Image = null;
            this.metroTextBoxElevePrenomSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxElevePrenomSelected.CustomButton.Name = "";
            this.metroTextBoxElevePrenomSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxElevePrenomSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxElevePrenomSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxElevePrenomSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxElevePrenomSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxElevePrenomSelected.CustomButton.Visible = false;
            this.metroTextBoxElevePrenomSelected.Lines = new string[0];
            this.metroTextBoxElevePrenomSelected.Location = new System.Drawing.Point(484, 221);
            this.metroTextBoxElevePrenomSelected.MaxLength = 32767;
            this.metroTextBoxElevePrenomSelected.Name = "metroTextBoxElevePrenomSelected";
            this.metroTextBoxElevePrenomSelected.PasswordChar = '\0';
            this.metroTextBoxElevePrenomSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxElevePrenomSelected.SelectedText = "";
            this.metroTextBoxElevePrenomSelected.SelectionLength = 0;
            this.metroTextBoxElevePrenomSelected.SelectionStart = 0;
            this.metroTextBoxElevePrenomSelected.ShortcutsEnabled = true;
            this.metroTextBoxElevePrenomSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxElevePrenomSelected.TabIndex = 26;
            this.metroTextBoxElevePrenomSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxElevePrenomSelected.UseSelectable = true;
            this.metroTextBoxElevePrenomSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxElevePrenomSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxEleveMoyenneSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Image = null;
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Name = "";
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxEleveMoyenneSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveMoyenneSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxEleveMoyenneSelected.CustomButton.Visible = false;
            this.metroTextBoxEleveMoyenneSelected.Lines = new string[0];
            this.metroTextBoxEleveMoyenneSelected.Location = new System.Drawing.Point(484, 355);
            this.metroTextBoxEleveMoyenneSelected.MaxLength = 32767;
            this.metroTextBoxEleveMoyenneSelected.Name = "metroTextBoxEleveMoyenneSelected";
            this.metroTextBoxEleveMoyenneSelected.PasswordChar = '\0';
            this.metroTextBoxEleveMoyenneSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxEleveMoyenneSelected.SelectedText = "";
            this.metroTextBoxEleveMoyenneSelected.SelectionLength = 0;
            this.metroTextBoxEleveMoyenneSelected.SelectionStart = 0;
            this.metroTextBoxEleveMoyenneSelected.ShortcutsEnabled = true;
            this.metroTextBoxEleveMoyenneSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxEleveMoyenneSelected.TabIndex = 27;
            this.metroTextBoxEleveMoyenneSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxEleveMoyenneSelected.UseSelectable = true;
            this.metroTextBoxEleveMoyenneSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxEleveMoyenneSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroDateTimeEleveDateNaissSelected
            // 
            this.metroDateTimeEleveDateNaissSelected.Location = new System.Drawing.Point(484, 260);
            this.metroDateTimeEleveDateNaissSelected.MinimumSize = new System.Drawing.Size(0, 30);
            this.metroDateTimeEleveDateNaissSelected.Name = "metroDateTimeEleveDateNaissSelected";
            this.metroDateTimeEleveDateNaissSelected.Size = new System.Drawing.Size(158, 30);
            this.metroDateTimeEleveDateNaissSelected.TabIndex = 28;
            // 
            // metroComboBoxEleveClasseSelected
            // 
            this.metroComboBoxEleveClasseSelected.FormattingEnabled = true;
            this.metroComboBoxEleveClasseSelected.ItemHeight = 24;
            this.metroComboBoxEleveClasseSelected.Location = new System.Drawing.Point(484, 394);
            this.metroComboBoxEleveClasseSelected.Name = "metroComboBoxEleveClasseSelected";
            this.metroComboBoxEleveClasseSelected.Size = new System.Drawing.Size(158, 30);
            this.metroComboBoxEleveClasseSelected.TabIndex = 29;
            this.metroComboBoxEleveClasseSelected.UseSelectable = true;
            // 
            // metroComboBoxEleveFormationSelected
            // 
            this.metroComboBoxEleveFormationSelected.FormattingEnabled = true;
            this.metroComboBoxEleveFormationSelected.ItemHeight = 24;
            this.metroComboBoxEleveFormationSelected.Location = new System.Drawing.Point(484, 307);
            this.metroComboBoxEleveFormationSelected.Name = "metroComboBoxEleveFormationSelected";
            this.metroComboBoxEleveFormationSelected.Size = new System.Drawing.Size(158, 30);
            this.metroComboBoxEleveFormationSelected.TabIndex = 30;
            this.metroComboBoxEleveFormationSelected.UseSelectable = true;
            // 
            // metroTextBoxClasseSigleSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxClasseSigleSelected.CustomButton.Image = null;
            this.metroTextBoxClasseSigleSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxClasseSigleSelected.CustomButton.Name = "";
            this.metroTextBoxClasseSigleSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxClasseSigleSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxClasseSigleSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxClasseSigleSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClasseSigleSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxClasseSigleSelected.CustomButton.Visible = false;
            this.metroTextBoxClasseSigleSelected.Lines = new string[0];
            this.metroTextBoxClasseSigleSelected.Location = new System.Drawing.Point(791, 231);
            this.metroTextBoxClasseSigleSelected.MaxLength = 32767;
            this.metroTextBoxClasseSigleSelected.Name = "metroTextBoxClasseSigleSelected";
            this.metroTextBoxClasseSigleSelected.PasswordChar = '\0';
            this.metroTextBoxClasseSigleSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxClasseSigleSelected.SelectedText = "";
            this.metroTextBoxClasseSigleSelected.SelectionLength = 0;
            this.metroTextBoxClasseSigleSelected.SelectionStart = 0;
            this.metroTextBoxClasseSigleSelected.ShortcutsEnabled = true;
            this.metroTextBoxClasseSigleSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxClasseSigleSelected.TabIndex = 31;
            this.metroTextBoxClasseSigleSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxClasseSigleSelected.UseSelectable = true;
            this.metroTextBoxClasseSigleSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxClasseSigleSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Classe";
            // 
            // metroLabelProfId
            // 
            this.metroLabelProfId.AutoSize = true;
            this.metroLabelProfId.Location = new System.Drawing.Point(399, 139);
            this.metroLabelProfId.Name = "metroLabelProfId";
            this.metroLabelProfId.Size = new System.Drawing.Size(51, 20);
            this.metroLabelProfId.TabIndex = 32;
            this.metroLabelProfId.Text = "Prof ID";
            this.metroLabelProfId.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelProfIdSelected
            // 
            this.metroLabelProfIdSelected.AutoSize = true;
            this.metroLabelProfIdSelected.Location = new System.Drawing.Point(484, 139);
            this.metroLabelProfIdSelected.Name = "metroLabelProfIdSelected";
            this.metroLabelProfIdSelected.Size = new System.Drawing.Size(34, 20);
            this.metroLabelProfIdSelected.TabIndex = 33;
            this.metroLabelProfIdSelected.Text = "N/A";
            this.metroLabelProfIdSelected.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelProfNom
            // 
            this.metroLabelProfNom.AutoSize = true;
            this.metroLabelProfNom.Location = new System.Drawing.Point(399, 183);
            this.metroLabelProfNom.Name = "metroLabelProfNom";
            this.metroLabelProfNom.Size = new System.Drawing.Size(40, 20);
            this.metroLabelProfNom.TabIndex = 34;
            this.metroLabelProfNom.Text = "Nom";
            this.metroLabelProfNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelProfPrenom
            // 
            this.metroLabelProfPrenom.AutoSize = true;
            this.metroLabelProfPrenom.Location = new System.Drawing.Point(401, 224);
            this.metroLabelProfPrenom.Name = "metroLabelProfPrenom";
            this.metroLabelProfPrenom.Size = new System.Drawing.Size(58, 20);
            this.metroLabelProfPrenom.TabIndex = 35;
            this.metroLabelProfPrenom.Text = "Prenom";
            this.metroLabelProfPrenom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelProfDateNaiss
            // 
            this.metroLabelProfDateNaiss.AutoSize = true;
            this.metroLabelProfDateNaiss.Location = new System.Drawing.Point(399, 270);
            this.metroLabelProfDateNaiss.Name = "metroLabelProfDateNaiss";
            this.metroLabelProfDateNaiss.Size = new System.Drawing.Size(75, 20);
            this.metroLabelProfDateNaiss.TabIndex = 36;
            this.metroLabelProfDateNaiss.Text = "Date Naiss";
            this.metroLabelProfDateNaiss.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxProfNomSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxProfNomSelected.CustomButton.Image = null;
            this.metroTextBoxProfNomSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxProfNomSelected.CustomButton.Name = "";
            this.metroTextBoxProfNomSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxProfNomSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxProfNomSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxProfNomSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfNomSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxProfNomSelected.CustomButton.Visible = false;
            this.metroTextBoxProfNomSelected.Lines = new string[0];
            this.metroTextBoxProfNomSelected.Location = new System.Drawing.Point(484, 180);
            this.metroTextBoxProfNomSelected.MaxLength = 32767;
            this.metroTextBoxProfNomSelected.Name = "metroTextBoxProfNomSelected";
            this.metroTextBoxProfNomSelected.PasswordChar = '\0';
            this.metroTextBoxProfNomSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxProfNomSelected.SelectedText = "";
            this.metroTextBoxProfNomSelected.SelectionLength = 0;
            this.metroTextBoxProfNomSelected.SelectionStart = 0;
            this.metroTextBoxProfNomSelected.ShortcutsEnabled = true;
            this.metroTextBoxProfNomSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxProfNomSelected.TabIndex = 37;
            this.metroTextBoxProfNomSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfNomSelected.UseSelectable = true;
            this.metroTextBoxProfNomSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxProfNomSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBoxProfPrenomSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxProfPrenomSelected.CustomButton.Image = null;
            this.metroTextBoxProfPrenomSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxProfPrenomSelected.CustomButton.Name = "";
            this.metroTextBoxProfPrenomSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxProfPrenomSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxProfPrenomSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxProfPrenomSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfPrenomSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxProfPrenomSelected.CustomButton.Visible = false;
            this.metroTextBoxProfPrenomSelected.Lines = new string[0];
            this.metroTextBoxProfPrenomSelected.Location = new System.Drawing.Point(484, 221);
            this.metroTextBoxProfPrenomSelected.MaxLength = 32767;
            this.metroTextBoxProfPrenomSelected.Name = "metroTextBoxProfPrenomSelected";
            this.metroTextBoxProfPrenomSelected.PasswordChar = '\0';
            this.metroTextBoxProfPrenomSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxProfPrenomSelected.SelectedText = "";
            this.metroTextBoxProfPrenomSelected.SelectionLength = 0;
            this.metroTextBoxProfPrenomSelected.SelectionStart = 0;
            this.metroTextBoxProfPrenomSelected.ShortcutsEnabled = true;
            this.metroTextBoxProfPrenomSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxProfPrenomSelected.TabIndex = 38;
            this.metroTextBoxProfPrenomSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfPrenomSelected.UseSelectable = true;
            this.metroTextBoxProfPrenomSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxProfPrenomSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroDateTimeProfDateNaissSelected
            // 
            this.metroDateTimeProfDateNaissSelected.Location = new System.Drawing.Point(484, 260);
            this.metroDateTimeProfDateNaissSelected.MinimumSize = new System.Drawing.Size(0, 30);
            this.metroDateTimeProfDateNaissSelected.Name = "metroDateTimeProfDateNaissSelected";
            this.metroDateTimeProfDateNaissSelected.Size = new System.Drawing.Size(158, 30);
            this.metroDateTimeProfDateNaissSelected.TabIndex = 39;
            // 
            // metroLabelProfStatut
            // 
            this.metroLabelProfStatut.AutoSize = true;
            this.metroLabelProfStatut.Location = new System.Drawing.Point(399, 317);
            this.metroLabelProfStatut.Name = "metroLabelProfStatut";
            this.metroLabelProfStatut.Size = new System.Drawing.Size(43, 20);
            this.metroLabelProfStatut.TabIndex = 40;
            this.metroLabelProfStatut.Text = "Statut";
            this.metroLabelProfStatut.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroComboBoxProfStatutSelected
            // 
            this.metroComboBoxProfStatutSelected.FormattingEnabled = true;
            this.metroComboBoxProfStatutSelected.ItemHeight = 24;
            this.metroComboBoxProfStatutSelected.Location = new System.Drawing.Point(484, 307);
            this.metroComboBoxProfStatutSelected.Name = "metroComboBoxProfStatutSelected";
            this.metroComboBoxProfStatutSelected.Size = new System.Drawing.Size(158, 30);
            this.metroComboBoxProfStatutSelected.TabIndex = 41;
            this.metroComboBoxProfStatutSelected.UseSelectable = true;
            // 
            // metroLabelProfSalaire
            // 
            this.metroLabelProfSalaire.AutoSize = true;
            this.metroLabelProfSalaire.Location = new System.Drawing.Point(399, 358);
            this.metroLabelProfSalaire.Name = "metroLabelProfSalaire";
            this.metroLabelProfSalaire.Size = new System.Drawing.Size(49, 20);
            this.metroLabelProfSalaire.TabIndex = 42;
            this.metroLabelProfSalaire.Text = "Salaire";
            this.metroLabelProfSalaire.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxProfSalaireSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxProfSalaireSelected.CustomButton.Image = null;
            this.metroTextBoxProfSalaireSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxProfSalaireSelected.CustomButton.Name = "";
            this.metroTextBoxProfSalaireSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxProfSalaireSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxProfSalaireSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxProfSalaireSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfSalaireSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxProfSalaireSelected.CustomButton.Visible = false;
            this.metroTextBoxProfSalaireSelected.Lines = new string[0];
            this.metroTextBoxProfSalaireSelected.Location = new System.Drawing.Point(484, 355);
            this.metroTextBoxProfSalaireSelected.MaxLength = 32767;
            this.metroTextBoxProfSalaireSelected.Name = "metroTextBoxProfSalaireSelected";
            this.metroTextBoxProfSalaireSelected.PasswordChar = '\0';
            this.metroTextBoxProfSalaireSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxProfSalaireSelected.SelectedText = "";
            this.metroTextBoxProfSalaireSelected.SelectionLength = 0;
            this.metroTextBoxProfSalaireSelected.SelectionStart = 0;
            this.metroTextBoxProfSalaireSelected.ShortcutsEnabled = true;
            this.metroTextBoxProfSalaireSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxProfSalaireSelected.TabIndex = 43;
            this.metroTextBoxProfSalaireSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxProfSalaireSelected.UseSelectable = true;
            this.metroTextBoxProfSalaireSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxProfSalaireSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButtonProfUpdate
            // 
            this.metroButtonProfUpdate.Location = new System.Drawing.Point(553, 383);
            this.metroButtonProfUpdate.Name = "metroButtonProfUpdate";
            this.metroButtonProfUpdate.Size = new System.Drawing.Size(89, 27);
            this.metroButtonProfUpdate.TabIndex = 44;
            this.metroButtonProfUpdate.Text = "Update";
            this.metroButtonProfUpdate.UseSelectable = true;
            this.metroButtonProfUpdate.Click += new System.EventHandler(this.metroButtonProfUpdate_Click);
            // 
            // metroLabelMatiereId
            // 
            this.metroLabelMatiereId.AutoSize = true;
            this.metroLabelMatiereId.Location = new System.Drawing.Point(399, 139);
            this.metroLabelMatiereId.Name = "metroLabelMatiereId";
            this.metroLabelMatiereId.Size = new System.Drawing.Size(73, 20);
            this.metroLabelMatiereId.TabIndex = 45;
            this.metroLabelMatiereId.Text = "Matiere ID";
            this.metroLabelMatiereId.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelMatiereIdSelected
            // 
            this.metroLabelMatiereIdSelected.AutoSize = true;
            this.metroLabelMatiereIdSelected.Location = new System.Drawing.Point(495, 139);
            this.metroLabelMatiereIdSelected.Name = "metroLabelMatiereIdSelected";
            this.metroLabelMatiereIdSelected.Size = new System.Drawing.Size(34, 20);
            this.metroLabelMatiereIdSelected.TabIndex = 46;
            this.metroLabelMatiereIdSelected.Text = "N/A";
            this.metroLabelMatiereIdSelected.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabelMatiereNom
            // 
            this.metroLabelMatiereNom.AutoSize = true;
            this.metroLabelMatiereNom.Location = new System.Drawing.Point(399, 183);
            this.metroLabelMatiereNom.Name = "metroLabelMatiereNom";
            this.metroLabelMatiereNom.Size = new System.Drawing.Size(40, 20);
            this.metroLabelMatiereNom.TabIndex = 47;
            this.metroLabelMatiereNom.Text = "Nom";
            this.metroLabelMatiereNom.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxMatiereNomSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxMatiereNomSelected.CustomButton.Image = null;
            this.metroTextBoxMatiereNomSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxMatiereNomSelected.CustomButton.Name = "";
            this.metroTextBoxMatiereNomSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxMatiereNomSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxMatiereNomSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxMatiereNomSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereNomSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxMatiereNomSelected.CustomButton.Visible = false;
            this.metroTextBoxMatiereNomSelected.Lines = new string[0];
            this.metroTextBoxMatiereNomSelected.Location = new System.Drawing.Point(495, 180);
            this.metroTextBoxMatiereNomSelected.MaxLength = 32767;
            this.metroTextBoxMatiereNomSelected.Name = "metroTextBoxMatiereNomSelected";
            this.metroTextBoxMatiereNomSelected.PasswordChar = '\0';
            this.metroTextBoxMatiereNomSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxMatiereNomSelected.SelectedText = "";
            this.metroTextBoxMatiereNomSelected.SelectionLength = 0;
            this.metroTextBoxMatiereNomSelected.SelectionStart = 0;
            this.metroTextBoxMatiereNomSelected.ShortcutsEnabled = true;
            this.metroTextBoxMatiereNomSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxMatiereNomSelected.TabIndex = 48;
            this.metroTextBoxMatiereNomSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereNomSelected.UseSelectable = true;
            this.metroTextBoxMatiereNomSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxMatiereNomSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelMatiereHeure
            // 
            this.metroLabelMatiereHeure.AutoSize = true;
            this.metroLabelMatiereHeure.Location = new System.Drawing.Point(399, 224);
            this.metroLabelMatiereHeure.Name = "metroLabelMatiereHeure";
            this.metroLabelMatiereHeure.Size = new System.Drawing.Size(81, 20);
            this.metroLabelMatiereHeure.TabIndex = 49;
            this.metroLabelMatiereHeure.Text = "Heures (Int)";
            this.metroLabelMatiereHeure.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBoxMatiereHeureSelected
            // 
            // 
            // 
            // 
            this.metroTextBoxMatiereHeureSelected.CustomButton.Image = null;
            this.metroTextBoxMatiereHeureSelected.CustomButton.Location = new System.Drawing.Point(136, 1);
            this.metroTextBoxMatiereHeureSelected.CustomButton.Name = "";
            this.metroTextBoxMatiereHeureSelected.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBoxMatiereHeureSelected.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBoxMatiereHeureSelected.CustomButton.TabIndex = 1;
            this.metroTextBoxMatiereHeureSelected.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereHeureSelected.CustomButton.UseSelectable = true;
            this.metroTextBoxMatiereHeureSelected.CustomButton.Visible = false;
            this.metroTextBoxMatiereHeureSelected.Lines = new string[0];
            this.metroTextBoxMatiereHeureSelected.Location = new System.Drawing.Point(495, 221);
            this.metroTextBoxMatiereHeureSelected.MaxLength = 32767;
            this.metroTextBoxMatiereHeureSelected.Name = "metroTextBoxMatiereHeureSelected";
            this.metroTextBoxMatiereHeureSelected.PasswordChar = '\0';
            this.metroTextBoxMatiereHeureSelected.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBoxMatiereHeureSelected.SelectedText = "";
            this.metroTextBoxMatiereHeureSelected.SelectionLength = 0;
            this.metroTextBoxMatiereHeureSelected.SelectionStart = 0;
            this.metroTextBoxMatiereHeureSelected.ShortcutsEnabled = true;
            this.metroTextBoxMatiereHeureSelected.Size = new System.Drawing.Size(158, 23);
            this.metroTextBoxMatiereHeureSelected.TabIndex = 50;
            this.metroTextBoxMatiereHeureSelected.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBoxMatiereHeureSelected.UseSelectable = true;
            this.metroTextBoxMatiereHeureSelected.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBoxMatiereHeureSelected.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabelMatiereClasse
            // 
            this.metroLabelMatiereClasse.AutoSize = true;
            this.metroLabelMatiereClasse.Location = new System.Drawing.Point(399, 270);
            this.metroLabelMatiereClasse.Name = "metroLabelMatiereClasse";
            this.metroLabelMatiereClasse.Size = new System.Drawing.Size(48, 20);
            this.metroLabelMatiereClasse.TabIndex = 51;
            this.metroLabelMatiereClasse.Text = "Classe";
            this.metroLabelMatiereClasse.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroComboBoxMatiereClasseSelected
            // 
            this.metroComboBoxMatiereClasseSelected.FormattingEnabled = true;
            this.metroComboBoxMatiereClasseSelected.ItemHeight = 24;
            this.metroComboBoxMatiereClasseSelected.Location = new System.Drawing.Point(495, 260);
            this.metroComboBoxMatiereClasseSelected.Name = "metroComboBoxMatiereClasseSelected";
            this.metroComboBoxMatiereClasseSelected.Size = new System.Drawing.Size(158, 30);
            this.metroComboBoxMatiereClasseSelected.TabIndex = 52;
            this.metroComboBoxMatiereClasseSelected.UseSelectable = true;
            // 
            // metroLabelMatiereProf
            // 
            this.metroLabelMatiereProf.AutoSize = true;
            this.metroLabelMatiereProf.Location = new System.Drawing.Point(399, 317);
            this.metroLabelMatiereProf.Name = "metroLabelMatiereProf";
            this.metroLabelMatiereProf.Size = new System.Drawing.Size(75, 20);
            this.metroLabelMatiereProf.TabIndex = 53;
            this.metroLabelMatiereProf.Text = "Professeur";
            this.metroLabelMatiereProf.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroComboBoxMatiereProfSelected
            // 
            this.metroComboBoxMatiereProfSelected.FormattingEnabled = true;
            this.metroComboBoxMatiereProfSelected.ItemHeight = 24;
            this.metroComboBoxMatiereProfSelected.Location = new System.Drawing.Point(495, 307);
            this.metroComboBoxMatiereProfSelected.Name = "metroComboBoxMatiereProfSelected";
            this.metroComboBoxMatiereProfSelected.Size = new System.Drawing.Size(158, 30);
            this.metroComboBoxMatiereProfSelected.TabIndex = 54;
            this.metroComboBoxMatiereProfSelected.UseSelectable = true;
            // 
            // metroButtonMatiereUpdate
            // 
            this.metroButtonMatiereUpdate.Location = new System.Drawing.Point(553, 355);
            this.metroButtonMatiereUpdate.Name = "metroButtonMatiereUpdate";
            this.metroButtonMatiereUpdate.Size = new System.Drawing.Size(89, 27);
            this.metroButtonMatiereUpdate.TabIndex = 55;
            this.metroButtonMatiereUpdate.Text = "Update";
            this.metroButtonMatiereUpdate.UseSelectable = true;
            this.metroButtonMatiereUpdate.Click += new System.EventHandler(this.metroButtonMatiereUpdate_Click);
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Classe";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Prof";
            // 
            // UPDATE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1048, 480);
            this.Controls.Add(this.metroButtonMatiereUpdate);
            this.Controls.Add(this.metroComboBoxMatiereProfSelected);
            this.Controls.Add(this.metroLabelMatiereProf);
            this.Controls.Add(this.metroComboBoxMatiereClasseSelected);
            this.Controls.Add(this.metroLabelMatiereClasse);
            this.Controls.Add(this.metroTextBoxMatiereHeureSelected);
            this.Controls.Add(this.metroLabelMatiereHeure);
            this.Controls.Add(this.metroTextBoxMatiereNomSelected);
            this.Controls.Add(this.metroLabelMatiereNom);
            this.Controls.Add(this.metroLabelMatiereIdSelected);
            this.Controls.Add(this.metroLabelMatiereId);
            this.Controls.Add(this.metroButtonProfUpdate);
            this.Controls.Add(this.metroTextBoxProfSalaireSelected);
            this.Controls.Add(this.metroLabelProfSalaire);
            this.Controls.Add(this.metroComboBoxProfStatutSelected);
            this.Controls.Add(this.metroLabelProfStatut);
            this.Controls.Add(this.metroDateTimeProfDateNaissSelected);
            this.Controls.Add(this.metroTextBoxProfPrenomSelected);
            this.Controls.Add(this.metroTextBoxProfNomSelected);
            this.Controls.Add(this.metroLabelProfDateNaiss);
            this.Controls.Add(this.metroLabelProfPrenom);
            this.Controls.Add(this.metroLabelProfNom);
            this.Controls.Add(this.metroLabelProfIdSelected);
            this.Controls.Add(this.metroLabelProfId);
            this.Controls.Add(this.metroTextBoxClasseSigleSelected);
            this.Controls.Add(this.metroComboBoxEleveFormationSelected);
            this.Controls.Add(this.metroComboBoxEleveClasseSelected);
            this.Controls.Add(this.metroDateTimeEleveDateNaissSelected);
            this.Controls.Add(this.metroTextBoxEleveMoyenneSelected);
            this.Controls.Add(this.metroTextBoxElevePrenomSelected);
            this.Controls.Add(this.metroTextBoxEleveNomSelected);
            this.Controls.Add(this.metroButtonEleveUpdate);
            this.Controls.Add(this.metroLabelEleveClasse);
            this.Controls.Add(this.metroLabelEleveMoyenne);
            this.Controls.Add(this.metroLabelEleveFormation);
            this.Controls.Add(this.metroLabelEleveDateNaiss);
            this.Controls.Add(this.metroLabelElevePrenom);
            this.Controls.Add(this.metroLabelEleveNom);
            this.Controls.Add(this.metroLabelEleveIdSelected);
            this.Controls.Add(this.metroLabelEleveId);
            this.Controls.Add(this.metroButtonClasseUpdate);
            this.Controls.Add(this.metroLabelClasseSigle);
            this.Controls.Add(this.metroLabelClasseIdSelected);
            this.Controls.Add(this.metroLabelClasseId);
            this.Controls.Add(this.metroListViewMatiere);
            this.Controls.Add(this.metroListViewEleve);
            this.Controls.Add(this.metroListViewProf);
            this.Controls.Add(this.metroTileMatiere);
            this.Controls.Add(this.metroTileEleve);
            this.Controls.Add(this.metroTileProf);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroListViewClasse);
            this.Controls.Add(this.metroTileClasse);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UPDATE";
            this.Text = "Update";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile metroTileClasse;
        private MetroFramework.Controls.MetroListView metroListViewClasse;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroTile metroTileProf;
        private MetroFramework.Controls.MetroTile metroTileEleve;
        private MetroFramework.Controls.MetroTile metroTileMatiere;
        private MetroFramework.Controls.MetroListView metroListViewProf;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private MetroFramework.Controls.MetroListView metroListViewEleve;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private MetroFramework.Controls.MetroListView metroListViewMatiere;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private MetroFramework.Controls.MetroLabel metroLabelClasseId;
        private MetroFramework.Controls.MetroLabel metroLabelClasseIdSelected;
        private MetroFramework.Controls.MetroLabel metroLabelClasseSigle;
        private MetroFramework.Controls.MetroButton metroButtonClasseUpdate;
        private MetroFramework.Controls.MetroLabel metroLabelEleveId;
        private MetroFramework.Controls.MetroLabel metroLabelEleveIdSelected;
        private MetroFramework.Controls.MetroLabel metroLabelEleveNom;
        private MetroFramework.Controls.MetroLabel metroLabelElevePrenom;
        private MetroFramework.Controls.MetroLabel metroLabelEleveDateNaiss;
        private MetroFramework.Controls.MetroLabel metroLabelEleveFormation;
        private MetroFramework.Controls.MetroLabel metroLabelEleveMoyenne;
        private MetroFramework.Controls.MetroLabel metroLabelEleveClasse;
        private MetroFramework.Controls.MetroButton metroButtonEleveUpdate;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEleveNomSelected;
        private MetroFramework.Controls.MetroTextBox metroTextBoxElevePrenomSelected;
        private MetroFramework.Controls.MetroTextBox metroTextBoxEleveMoyenneSelected;
        private MetroFramework.Controls.MetroDateTime metroDateTimeEleveDateNaissSelected;
        private MetroFramework.Controls.MetroComboBox metroComboBoxEleveClasseSelected;
        private MetroFramework.Controls.MetroComboBox metroComboBoxEleveFormationSelected;
        private MetroFramework.Controls.MetroTextBox metroTextBoxClasseSigleSelected;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private MetroFramework.Controls.MetroLabel metroLabelProfId;
        private MetroFramework.Controls.MetroLabel metroLabelProfIdSelected;
        private MetroFramework.Controls.MetroLabel metroLabelProfNom;
        private MetroFramework.Controls.MetroLabel metroLabelProfPrenom;
        private MetroFramework.Controls.MetroLabel metroLabelProfDateNaiss;
        private MetroFramework.Controls.MetroTextBox metroTextBoxProfNomSelected;
        private MetroFramework.Controls.MetroTextBox metroTextBoxProfPrenomSelected;
        private MetroFramework.Controls.MetroDateTime metroDateTimeProfDateNaissSelected;
        private MetroFramework.Controls.MetroLabel metroLabelProfStatut;
        private MetroFramework.Controls.MetroComboBox metroComboBoxProfStatutSelected;
        private MetroFramework.Controls.MetroLabel metroLabelProfSalaire;
        private MetroFramework.Controls.MetroTextBox metroTextBoxProfSalaireSelected;
        private MetroFramework.Controls.MetroButton metroButtonProfUpdate;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereId;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereIdSelected;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereNom;
        private MetroFramework.Controls.MetroTextBox metroTextBoxMatiereNomSelected;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereHeure;
        private MetroFramework.Controls.MetroTextBox metroTextBoxMatiereHeureSelected;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereClasse;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMatiereClasseSelected;
        private MetroFramework.Controls.MetroLabel metroLabelMatiereProf;
        private MetroFramework.Controls.MetroComboBox metroComboBoxMatiereProfSelected;
        private MetroFramework.Controls.MetroButton metroButtonMatiereUpdate;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
    }
}

