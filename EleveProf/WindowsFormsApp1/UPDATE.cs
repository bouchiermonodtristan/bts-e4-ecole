﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EleveProfLibrary;

namespace WindowsFormsApp1
{
    public partial class UPDATE : MetroFramework.Forms.MetroForm
    {
        public UPDATE()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hideAllList();
            hideAllForms();
        }
        private void hideAllList()
        {
            metroListViewClasse.Hide();
            metroListViewProf.Hide();
            metroListViewEleve.Hide();
            metroListViewMatiere.Hide();
        }
        private void hideAllForms()
        {
            hideClasseForms();
            hideProfForms();
            hideMatiereForms();
            hideEleveForms();
        }
        private void showClasseForms()
        {
            metroLabelClasseId.Show();
            metroLabelClasseIdSelected.Show();
            metroLabelClasseSigle.Show();
            metroButtonClasseUpdate.Show();
            metroTextBoxClasseSigleSelected.Show();
        }
        private void showProfForms()
        {
            metroTextBoxProfSalaireSelected.Show();
            metroTextBoxProfNomSelected.Show();
            metroTextBoxProfPrenomSelected.Show();
            metroButtonProfUpdate.Show();
            metroLabelProfDateNaiss.Show();
            metroLabelProfStatut.Show();
            metroLabelProfId.Show();
            metroLabelProfIdSelected.Show();
            metroLabelProfSalaire.Show();
            metroLabelProfNom.Show();
            metroLabelProfPrenom.Show();
            metroDateTimeProfDateNaissSelected.Show();


            metroComboBoxProfStatutSelected.Show();
            metroComboBoxProfStatutSelected.Items.Clear();
            metroComboBoxProfStatutSelected.Items.Add("Intervenant");
            metroComboBoxProfStatutSelected.Items.Add("Permanent");

        }
        private void showMatiereForms()
        {
            metroButtonMatiereUpdate.Show();
            metroComboBoxMatiereClasseSelected.Show();
            metroComboBoxMatiereProfSelected.Show();
            metroLabelMatiereClasse.Show();
            metroLabelMatiereHeure.Show();
            metroLabelMatiereId.Show();
            metroLabelMatiereIdSelected.Show();
            metroLabelMatiereNom.Show();
            metroLabelMatiereProf.Show();
            metroTextBoxMatiereHeureSelected.Show();
            metroTextBoxMatiereNomSelected.Show();

            metroComboBoxMatiereClasseSelected.Items.Clear();
            metroComboBoxMatiereProfSelected.Items.Clear();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                metroComboBoxMatiereClasseSelected.Items.Add(classe.Sigle);
            }


            GestionPersonneADO gestionPersonne = new GestionPersonneADO();
            List<Prof> listProfs = gestionPersonne.ChargerProfs();
            foreach (Prof prof in listProfs)
            {
                metroComboBoxMatiereProfSelected.Items.Add(prof.Nom + " " + prof.Prenom + " $" + prof.Id);
            }

        }
        private void showEleveForms()
        {
            metroTextBoxEleveMoyenneSelected.Show();
            metroTextBoxEleveNomSelected.Show();
            metroTextBoxElevePrenomSelected.Show();
            metroButtonEleveUpdate.Show();
            metroLabelEleveClasse.Show();
            metroLabelEleveDateNaiss.Show();
            metroLabelEleveFormation.Show();
            metroLabelEleveId.Show();
            metroLabelEleveIdSelected.Show();
            metroLabelEleveMoyenne.Show();
            metroLabelEleveNom.Show();
            metroLabelElevePrenom.Show();
            metroDateTimeEleveDateNaissSelected.Show();


            metroComboBoxEleveFormationSelected.Show();
            metroComboBoxEleveFormationSelected.Items.Clear();
            metroComboBoxEleveFormationSelected.Items.Add("Initiale");
            metroComboBoxEleveFormationSelected.Items.Add("Alternance");

            metroComboBoxEleveClasseSelected.Show();
            metroComboBoxEleveClasseSelected.Items.Clear();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                metroComboBoxEleveClasseSelected.Items.Add(classe.Sigle);
            }
        }

        private void hideClasseForms()
        {
            metroLabelClasseId.Hide();
            metroLabelClasseIdSelected.Hide();
            metroLabelClasseSigle.Hide();
            metroButtonClasseUpdate.Hide();
            metroTextBoxClasseSigleSelected.Hide();
        }
        private void hideProfForms()
        {
            metroTextBoxProfSalaireSelected.Hide();
            metroTextBoxProfNomSelected.Hide();
            metroTextBoxProfPrenomSelected.Hide();
            metroButtonProfUpdate.Hide();
            metroLabelProfDateNaiss.Hide();
            metroLabelProfStatut.Hide();
            metroLabelProfId.Hide();
            metroLabelProfIdSelected.Hide();
            metroLabelProfSalaire.Hide();
            metroLabelProfNom.Hide();
            metroLabelProfPrenom.Hide();
            metroDateTimeProfDateNaissSelected.Hide();
            metroComboBoxProfStatutSelected.Hide();

        }
        private void hideMatiereForms()
        {
            metroButtonMatiereUpdate.Hide();
            metroComboBoxMatiereClasseSelected.Hide();
            metroComboBoxMatiereProfSelected.Hide();
            metroLabelMatiereClasse.Hide();
            metroLabelMatiereHeure.Hide();
            metroLabelMatiereId.Hide();
            metroLabelMatiereIdSelected.Hide();
            metroLabelMatiereNom.Hide();
            metroLabelMatiereProf.Hide();
            metroTextBoxMatiereHeureSelected.Hide();
            metroTextBoxMatiereNomSelected.Hide();
        }
        private void hideEleveForms()
        {
            metroTextBoxEleveMoyenneSelected.Hide();
            metroTextBoxEleveNomSelected.Hide();
            metroTextBoxElevePrenomSelected.Hide();
            metroComboBoxEleveClasseSelected.Hide();
            metroComboBoxEleveFormationSelected.Hide();
            metroButtonEleveUpdate.Hide();
            metroLabelEleveClasse.Hide();
            metroLabelEleveDateNaiss.Hide();
            metroLabelEleveFormation.Hide();
            metroLabelEleveId.Hide();
            metroLabelEleveIdSelected.Hide();
            metroLabelEleveMoyenne.Hide();
            metroLabelEleveNom.Hide();
            metroLabelElevePrenom.Hide();
            metroDateTimeEleveDateNaissSelected.Hide();
        }

        #region Views_Click
        private void metroTileClasse_Click(object sender, EventArgs e)
        {
            //MetroFramework.MetroMessageBox.Show(this, "WARNING", "MESSAGE BOX", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            hideAllList();
            hideAllForms();
            showClasseForms();
            metroListViewClasse.Items.Clear();
            metroListViewClasse.Show();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                ListViewItem item = new ListViewItem(classe.Id.ToString());
                item.SubItems.Add(classe.Sigle);
                metroListViewClasse.Items.Add(item);
            }


        }
        private void metroTileProf_Click(object sender, EventArgs e)
        {
            hideAllList();
            hideAllForms();
            showProfForms();
            metroListViewProf.Items.Clear();
            metroListViewProf.Show();
            GestionPersonneADO gestionProf = new GestionPersonneADO();
            List<Prof> listProfs = gestionProf.ChargerProfs();
            foreach (Prof prof in listProfs)
            {
                ListViewItem item = new ListViewItem(prof.Id.ToString());
                item.SubItems.Add(prof.Nom);
                item.SubItems.Add(prof.Prenom);
                item.SubItems.Add(prof.DateDeNaissance.ToString());
                item.SubItems.Add(prof.Statut.ToString());
                item.SubItems.Add(prof.Salaire.ToString());
                metroListViewProf.Items.Add(item);
            }
        }
        private void metroTileEleve_Click(object sender, EventArgs e)
        {
            hideAllList();
            hideAllForms();
            showEleveForms();
            metroListViewEleve.Items.Clear();
            metroListViewEleve.Show();
            GestionPersonneADO gestionEleve = new GestionPersonneADO();
            List<Eleve> listEleves = gestionEleve.ChargerEleves();
            foreach (Eleve eleve in listEleves)
            {
                ListViewItem item = new ListViewItem(eleve.Id.ToString());
                item.SubItems.Add(eleve.Nom);
                item.SubItems.Add(eleve.Prenom);
                item.SubItems.Add(eleve.DateDeNaissance.ToString());
                item.SubItems.Add(eleve.Formation.ToString());
                item.SubItems.Add(eleve.Moyenne.ToString());
                item.SubItems.Add(eleve.ClasseEleve.Id.ToString());
                metroListViewEleve.Items.Add(item);
            }
        }
        private void metroTileMatiere_Click(object sender, EventArgs e)
        {
            hideAllList();
            hideAllForms();
            showMatiereForms();
            metroListViewMatiere.Items.Clear();
            metroListViewMatiere.Show();
            GestionMatiereADO gestionMatiere = new GestionMatiereADO();
            List<Matiere> listMatieres = gestionMatiere.ChargerMatieres();
            foreach (Matiere matiere in listMatieres)
            {
                ListViewItem item = new ListViewItem(matiere.Id.ToString());
                item.SubItems.Add(matiere.Nom);
                item.SubItems.Add(matiere.Heure.ToString());
                item.SubItems.Add(matiere.ClasseAyantMatiere.Id.ToString());
                item.SubItems.Add(matiere.ProfDeMatiere.Id.ToString());
                metroListViewMatiere.Items.Add(item);
            }
        }
        #endregion
        #region Menu Switch
        //Switch to Views
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form View = new VIEW();
            View.ShowDialog();
            this.Close();
        }
        //Switch to ADD
        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Add = new ADD();
            Add.ShowDialog();
            this.Close();
        }
        //Switch to UPDATE
        private void metroButton3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Update = new UPDATE();
            Update.ShowDialog();
            this.Close();
        }
        //Switch to DELETE
        private void metroButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Delete = new DELETE();
            Delete.ShowDialog();
            this.Close();
        }
        #endregion

        private void UPDATE_Load(object sender, EventArgs e)
        {

        }

        private void metroListViewMatiere_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroLabelMatiereIdSelected.Text = metroListViewMatiere.SelectedItems[0].SubItems[0].Text;
            metroTextBoxMatiereNomSelected.Text = metroListViewMatiere.SelectedItems[0].SubItems[1].Text;
            metroTextBoxMatiereHeureSelected.Text = metroListViewMatiere.SelectedItems[0].SubItems[2].Text;

            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                if (metroListViewMatiere.SelectedItems[0].SubItems[3].Text == classe.Id.ToString())
                {
                    metroComboBoxMatiereClasseSelected.Text = classe.Sigle;
                }
            }

            GestionPersonneADO gestionPersonne = new GestionPersonneADO();
            List<Prof> listProfs = gestionPersonne.ChargerProfs();

            string SelectedID = metroListViewMatiere.SelectedItems[0].SubItems[4].Text;
            foreach (Prof prof in listProfs)
            {
                if (SelectedID == prof.Id.ToString())
                {
                    metroComboBoxMatiereProfSelected.Text = prof.Nom + " " + prof.Prenom + " $" + prof.Id; ;
                }
            }

           

        }
        private void metroListViewProf_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroLabelProfIdSelected.Text = metroListViewProf.SelectedItems[0].SubItems[0].Text;
            metroTextBoxProfNomSelected.Text = metroListViewProf.SelectedItems[0].SubItems[1].Text;
            metroTextBoxProfPrenomSelected.Text = metroListViewProf.SelectedItems[0].SubItems[2].Text;
            metroDateTimeProfDateNaissSelected.Text = metroListViewProf.SelectedItems[0].SubItems[3].Text;
            metroComboBoxProfStatutSelected.Text = metroListViewProf.SelectedItems[0].SubItems[4].Text;
            metroTextBoxProfSalaireSelected.Text = metroListViewProf.SelectedItems[0].SubItems[5].Text;
        }
        private void metroListViewClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroLabelClasseIdSelected.Text = metroListViewClasse.SelectedItems[0].SubItems[0].Text;
            metroTextBoxClasseSigleSelected.Text = metroListViewClasse.SelectedItems[0].SubItems[1].Text;
        }
        private void metroListViewEleve_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroLabelEleveIdSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[0].Text;
            metroTextBoxEleveNomSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[1].Text;
            metroTextBoxElevePrenomSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[2].Text;
            metroDateTimeEleveDateNaissSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[3].Text;
            metroComboBoxEleveFormationSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[4].Text;
            metroTextBoxEleveMoyenneSelected.Text = metroListViewEleve.SelectedItems[0].SubItems[5].Text;

            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                if (metroListViewEleve.SelectedItems[0].SubItems[6].Text == classe.Id.ToString())
                {
                    metroComboBoxEleveClasseSelected.Text = classe.Sigle;
                }
            }
        }


        #region UPDATE
        private void metroButtonEleveUpdate_Click(object sender, EventArgs e)
        {
            float Moyenne = 0;
            if (metroTextBoxEleveNomSelected.Text != "" && metroTextBoxEleveNomSelected.Text.Length > 2
                && metroTextBoxElevePrenomSelected.Text != "" && metroTextBoxElevePrenomSelected.Text.Length > 2
                && metroDateTimeEleveDateNaissSelected.Value != null
                && float.TryParse(metroTextBoxEleveMoyenneSelected.Text, out Moyenne) == true)
            {
                try
                {
                    Classe EleveClasse = new Classe(1, "TEST");
                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    List<Classe> listClasses = gestionClasse.ChargerClasses();
                    foreach (Classe classe in listClasses)
                    {
                        if(metroComboBoxEleveClasseSelected.Text == classe.Sigle)
                        {
                            EleveClasse = classe;
                        }
                    }

                    Eleve.TypeFormation Formation;
                    if (metroComboBoxEleveFormationSelected.Text == "Alternance")
                    {
                        Formation = Eleve.TypeFormation.Alternance;
                    }
                    else
                    {
                        Formation = Eleve.TypeFormation.Initiale;
                    }

                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    int id = 0;
                    Int32.TryParse(metroLabelEleveIdSelected.Text, out id);
                    Eleve NewEleve = new Eleve(
                        id, 
                        metroTextBoxEleveNomSelected.Text, 
                        metroTextBoxElevePrenomSelected.Text,
                        Convert.ToDateTime(metroDateTimeEleveDateNaissSelected.Text),
                        Formation,
                        Moyenne,
                        EleveClasse
                        );
                    gestionPersonne.ModifierEleve(NewEleve);
                    MetroFramework.MetroMessageBox.Show(this, "OK", "ELEVE SUCCESSFULLY UPDATED " + NewEleve.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On UPDATE SIGLE");
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void metroButtonClasseUpdate_Click(object sender, EventArgs e)
        {
            if (metroTextBoxClasseSigleSelected.Text != "" && metroTextBoxClasseSigleSelected.Text.Length < 6)
            {
                try
                {
                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    int id = 0;
                    Int32.TryParse(metroLabelClasseIdSelected.Text, out id);
                    Classe NewClasse = new Classe(id, metroTextBoxClasseSigleSelected.Text);
                    gestionClasse.ModifierClasse(NewClasse);
                    MetroFramework.MetroMessageBox.Show(this, "OK", "CLASSE SUCCESSFULLY UPDATED " + NewClasse.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On UPDATE SIGLE with value : " + metroTextBoxClasseSigleSelected.Text);
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void metroButtonProfUpdate_Click(object sender, EventArgs e)
        {
            float Salaire = 0;
            if (metroTextBoxProfNomSelected.Text != "" && metroTextBoxProfNomSelected.Text.Length > 2
                && metroTextBoxProfPrenomSelected.Text != "" && metroTextBoxProfPrenomSelected.Text.Length > 2
                && metroDateTimeProfDateNaissSelected.Value != null
                && float.TryParse(metroTextBoxProfSalaireSelected.Text, out Salaire) == true)
            {
                try
                {
                    Prof.TypeStatut Status;
                    if (metroComboBoxProfStatutSelected.Text == "Permanent")
                    {
                        Status = Prof.TypeStatut.Permanent;
                    }
                    else
                    {
                        Status = Prof.TypeStatut.Intervenant;
                    }

                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    int id = 0;
                    Int32.TryParse(metroLabelProfIdSelected.Text, out id);
                    Prof NewProf = new Prof(id,
                        metroTextBoxProfNomSelected.Text,
                        metroTextBoxProfPrenomSelected.Text,
                        Convert.ToDateTime(metroDateTimeProfDateNaissSelected.Text),
                        Status,
                        Salaire
                        );
                    gestionPersonne.ModifierProf(NewProf);
                    MetroFramework.MetroMessageBox.Show(this, "OK", "PROF SUCCESSFULLY UPDATED " + NewProf.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On UPDATE");
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void metroButtonMatiereUpdate_Click(object sender, EventArgs e)
        {
            int Heures = 0;
            if (metroTextBoxMatiereHeureSelected.Text != "" && int.TryParse(metroTextBoxMatiereHeureSelected.Text, out Heures) == true
                && metroTextBoxMatiereNomSelected.Text.Length > 1)
            {
                try
                {
                    GestionMatiereADO gestionMatiere = new GestionMatiereADO();
                    int idMatiere = 0;
                    string idStringProf = metroComboBoxMatiereProfSelected.Text.Split('$')[1];
                    int idProf = 0;

                    
                    Int32.TryParse(metroLabelMatiereIdSelected.Text, out idMatiere);
                    Int32.TryParse(idStringProf, out idProf);

                    Classe MatiereClasse = new Classe(1, "TEST");
                    GestionClasseADO gestionClasse = new GestionClasseADO();
                    List<Classe> listClasses = gestionClasse.ChargerClasses();
                    foreach (Classe classe in listClasses)
                    {
                        if (metroComboBoxMatiereClasseSelected.Text == classe.Sigle)
                        {
                            MatiereClasse = classe;
                        }
                    }

                    Prof MatiereProf = new Prof(1, "TEST", "TEST", DateTime.Now, 9999);
                    GestionPersonneADO gestionPersonne = new GestionPersonneADO();
                    List<Prof> listProfs = gestionPersonne.ChargerProfs();
                    foreach (Prof prof in listProfs)
                    {
                        if (idProf == prof.Id)
                        {
                            MatiereProf = prof;
                        }
                    }

                    Matiere NewMatiere = new Matiere(
                        idMatiere,
                        metroTextBoxMatiereNomSelected.Text,
                        Heures,
                        MatiereProf,
                        MatiereClasse
                        );
                    gestionMatiere.ModifierMatiere(NewMatiere);
                    MetroFramework.MetroMessageBox.Show(this, "OK", "MATIERE SUCCESSFULLY UPDATED " + NewMatiere.Id, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error : " + ex + " On UPDATE");
                }
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "WARNING", "An error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

    }
}
