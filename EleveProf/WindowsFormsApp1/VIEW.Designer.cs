﻿namespace WindowsFormsApp1
{
    partial class VIEW
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTileClasse = new MetroFramework.Controls.MetroTile();
            this.metroListViewClasse = new MetroFramework.Controls.MetroListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroTileProf = new MetroFramework.Controls.MetroTile();
            this.metroTileEleve = new MetroFramework.Controls.MetroTile();
            this.metroTileMatiere = new MetroFramework.Controls.MetroTile();
            this.metroListViewProf = new MetroFramework.Controls.MetroListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroListViewEleve = new MetroFramework.Controls.MetroListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.metroListViewMatiere = new MetroFramework.Controls.MetroListView();
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // metroTileClasse
            // 
            this.metroTileClasse.ActiveControl = null;
            this.metroTileClasse.Location = new System.Drawing.Point(694, 64);
            this.metroTileClasse.Name = "metroTileClasse";
            this.metroTileClasse.Size = new System.Drawing.Size(151, 44);
            this.metroTileClasse.TabIndex = 0;
            this.metroTileClasse.Text = "Generate Classe";
            this.metroTileClasse.UseSelectable = true;
            this.metroTileClasse.Click += new System.EventHandler(this.metroTileClasse_Click);
            // 
            // metroListViewClasse
            // 
            this.metroListViewClasse.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.metroListViewClasse.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewClasse.FullRowSelect = true;
            this.metroListViewClasse.Location = new System.Drawing.Point(23, 107);
            this.metroListViewClasse.Name = "metroListViewClasse";
            this.metroListViewClasse.OwnerDraw = true;
            this.metroListViewClasse.Size = new System.Drawing.Size(352, 305);
            this.metroListViewClasse.TabIndex = 1;
            this.metroListViewClasse.UseCompatibleStateImageBehavior = false;
            this.metroListViewClasse.UseSelectable = true;
            this.metroListViewClasse.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Classe iD";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Sigle";
            this.columnHeader2.Width = 100;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(24, 64);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(119, 37);
            this.metroButton1.TabIndex = 2;
            this.metroButton1.Text = "View";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(149, 64);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(119, 37);
            this.metroButton2.TabIndex = 3;
            this.metroButton2.Text = "Add";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(274, 63);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(119, 37);
            this.metroButton3.TabIndex = 4;
            this.metroButton3.Text = "Update";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(399, 63);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(119, 37);
            this.metroButton4.TabIndex = 5;
            this.metroButton4.Text = "Delete";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroTileProf
            // 
            this.metroTileProf.ActiveControl = null;
            this.metroTileProf.Location = new System.Drawing.Point(694, 114);
            this.metroTileProf.Name = "metroTileProf";
            this.metroTileProf.Size = new System.Drawing.Size(151, 45);
            this.metroTileProf.TabIndex = 0;
            this.metroTileProf.Text = "Generate Prof";
            this.metroTileProf.UseSelectable = true;
            this.metroTileProf.Click += new System.EventHandler(this.metroTileProf_Click);
            // 
            // metroTileEleve
            // 
            this.metroTileEleve.ActiveControl = null;
            this.metroTileEleve.Location = new System.Drawing.Point(851, 114);
            this.metroTileEleve.Name = "metroTileEleve";
            this.metroTileEleve.Size = new System.Drawing.Size(151, 45);
            this.metroTileEleve.TabIndex = 6;
            this.metroTileEleve.Text = "Generate Eleve";
            this.metroTileEleve.UseSelectable = true;
            this.metroTileEleve.Click += new System.EventHandler(this.metroTileEleve_Click);
            // 
            // metroTileMatiere
            // 
            this.metroTileMatiere.ActiveControl = null;
            this.metroTileMatiere.Location = new System.Drawing.Point(852, 64);
            this.metroTileMatiere.Name = "metroTileMatiere";
            this.metroTileMatiere.Size = new System.Drawing.Size(150, 44);
            this.metroTileMatiere.TabIndex = 7;
            this.metroTileMatiere.Text = "Generate Matiere";
            this.metroTileMatiere.UseSelectable = true;
            this.metroTileMatiere.Click += new System.EventHandler(this.metroTileMatiere_Click);
            // 
            // metroListViewProf
            // 
            this.metroListViewProf.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.metroListViewProf.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewProf.FullRowSelect = true;
            this.metroListViewProf.Location = new System.Drawing.Point(24, 107);
            this.metroListViewProf.Name = "metroListViewProf";
            this.metroListViewProf.OwnerDraw = true;
            this.metroListViewProf.Size = new System.Drawing.Size(636, 305);
            this.metroListViewProf.TabIndex = 8;
            this.metroListViewProf.UseCompatibleStateImageBehavior = false;
            this.metroListViewProf.UseSelectable = true;
            this.metroListViewProf.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Prof ID";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Nom";
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Prenom";
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "DateNaiss";
            this.columnHeader6.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Statut";
            this.columnHeader7.Width = 100;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Salaire";
            this.columnHeader8.Width = 100;
            // 
            // metroListViewEleve
            // 
            this.metroListViewEleve.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.metroListViewEleve.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewEleve.FullRowSelect = true;
            this.metroListViewEleve.Location = new System.Drawing.Point(23, 106);
            this.metroListViewEleve.Name = "metroListViewEleve";
            this.metroListViewEleve.OwnerDraw = true;
            this.metroListViewEleve.Size = new System.Drawing.Size(637, 304);
            this.metroListViewEleve.TabIndex = 9;
            this.metroListViewEleve.UseCompatibleStateImageBehavior = false;
            this.metroListViewEleve.UseSelectable = true;
            this.metroListViewEleve.View = System.Windows.Forms.View.Details;

            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "ID";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Nom";
            this.columnHeader10.Width = 100;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Prenom";
            this.columnHeader11.Width = 100;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "DateNaiss";
            this.columnHeader12.Width = 100;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Formation";
            this.columnHeader13.Width = 109;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Moyenne";
            this.columnHeader14.Width = 100;
            // 
            // metroListViewMatiere
            // 
            this.metroListViewMatiere.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17});
            this.metroListViewMatiere.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.metroListViewMatiere.FullRowSelect = true;
            this.metroListViewMatiere.Location = new System.Drawing.Point(23, 106);
            this.metroListViewMatiere.Name = "metroListViewMatiere";
            this.metroListViewMatiere.OwnerDraw = true;
            this.metroListViewMatiere.Size = new System.Drawing.Size(334, 306);
            this.metroListViewMatiere.TabIndex = 10;
            this.metroListViewMatiere.UseCompatibleStateImageBehavior = false;
            this.metroListViewMatiere.UseSelectable = true;
            this.metroListViewMatiere.View = System.Windows.Forms.View.Details;
            this.metroListViewMatiere.SelectedIndexChanged += new System.EventHandler(this.metroListViewMatiere_SelectedIndexChanged);
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "ID";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Matiere";
            this.columnHeader16.Width = 120;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Heures";
            this.columnHeader17.Width = 120;
            // 
            // VIEW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1048, 480);
            this.Controls.Add(this.metroListViewMatiere);
            this.Controls.Add(this.metroListViewEleve);
            this.Controls.Add(this.metroListViewProf);
            this.Controls.Add(this.metroTileMatiere);
            this.Controls.Add(this.metroTileEleve);
            this.Controls.Add(this.metroTileProf);
            this.Controls.Add(this.metroButton4);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroListViewClasse);
            this.Controls.Add(this.metroTileClasse);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "VIEW";
            this.Text = "Views";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile metroTileClasse;
        private MetroFramework.Controls.MetroListView metroListViewClasse;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroTile metroTileProf;
        private MetroFramework.Controls.MetroTile metroTileEleve;
        private MetroFramework.Controls.MetroTile metroTileMatiere;
        private MetroFramework.Controls.MetroListView metroListViewProf;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private MetroFramework.Controls.MetroListView metroListViewEleve;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private MetroFramework.Controls.MetroListView metroListViewMatiere;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
    }
}

