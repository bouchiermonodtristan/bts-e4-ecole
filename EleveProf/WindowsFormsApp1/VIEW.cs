﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EleveProfLibrary;

namespace WindowsFormsApp1
{
    public partial class VIEW : MetroFramework.Forms.MetroForm
    {
        public VIEW()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            hideAllList();
        }
        private void hideAllList()
        {
            metroListViewClasse.Hide();
            metroListViewProf.Hide();
            metroListViewEleve.Hide();
            metroListViewMatiere.Hide();
        }

        #region Views_Click
        private void metroTileClasse_Click(object sender, EventArgs e)
        {
            //MetroFramework.MetroMessageBox.Show(this, "WARNING", "MESSAGE BOX", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            hideAllList();
            metroListViewClasse.Items.Clear();
            metroListViewClasse.Show();
            GestionClasseADO gestionClasse = new GestionClasseADO();
            List<Classe> listClasses = gestionClasse.ChargerClasses();
            foreach (Classe classe in listClasses)
            {
                ListViewItem item = new ListViewItem(classe.Id.ToString());
                item.SubItems.Add(classe.Sigle);
                metroListViewClasse.Items.Add(item);
            }
        }
        private void metroTileProf_Click(object sender, EventArgs e)
        {
            hideAllList();
            metroListViewProf.Items.Clear();
            metroListViewProf.Show();
            GestionPersonneADO gestionProf = new GestionPersonneADO();
            List<Prof> listProfs = gestionProf.ChargerProfs();
            foreach (Prof prof in listProfs)
            {
                ListViewItem item = new ListViewItem(prof.Id.ToString());
                item.SubItems.Add(prof.Nom);
                item.SubItems.Add(prof.Prenom);
                item.SubItems.Add(prof.DateDeNaissance.ToString());
                item.SubItems.Add(prof.Statut.ToString());
                item.SubItems.Add(prof.Salaire.ToString());
                metroListViewProf.Items.Add(item);
            }
        }
        private void metroTileEleve_Click(object sender, EventArgs e)
        {
            hideAllList();
            metroListViewEleve.Items.Clear();
            metroListViewEleve.Show();
            GestionPersonneADO gestionEleve = new GestionPersonneADO();
            List<Eleve> listEleves = gestionEleve.ChargerEleves();
            foreach (Eleve eleve in listEleves)
            { 
                ListViewItem item = new ListViewItem(eleve.Id.ToString());
                item.SubItems.Add(eleve.Nom);
                item.SubItems.Add(eleve.Prenom);
                item.SubItems.Add(eleve.DateDeNaissance.ToString());
                item.SubItems.Add(eleve.Formation.ToString());
                item.SubItems.Add(eleve.Moyenne.ToString());
                metroListViewEleve.Items.Add(item);
            }
        }
        private void metroTileMatiere_Click(object sender, EventArgs e)
        {
            hideAllList();
            metroListViewMatiere.Items.Clear();
            metroListViewMatiere.Show();
            GestionMatiereADO gestionMatiere = new GestionMatiereADO();
            List<Matiere> listMatieres = gestionMatiere.ChargerMatieres();
            foreach (Matiere matiere in listMatieres)
            {
                ListViewItem item = new ListViewItem(matiere.Id.ToString());
                item.SubItems.Add(matiere.Nom);
                item.SubItems.Add(matiere.Heure.ToString());
                metroListViewMatiere.Items.Add(item);
            }
        }
        #endregion

        #region Menu Switch
        //Switch to Views
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form View = new VIEW();
            View.ShowDialog();
            this.Close();
        }
        //Switch to ADD
        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Add = new ADD();
            Add.ShowDialog();
            this.Close();
        }
        //Switch to UPDATE
        private void metroButton3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Update = new UPDATE();
            Update.ShowDialog();
            this.Close();
        }
        //Switch to DELETE
        private void metroButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form Delete = new DELETE();
            Delete.ShowDialog();
            this.Close();
        }
        #endregion

        private void metroListViewMatiere_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
